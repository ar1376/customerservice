package com.rayanen.java.ee.core.ws;

import com.rayanen.java.ee.core.dto.RespondDTO;
import com.rayanen.java.ee.core.facade.ICustomerFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.rayanen.java.ee.core.log.Log;


@RestController
@RequestMapping(value = "/backup")
public class BackupRestController {

    private final ICustomerFacade customerFacade;

    private final Log Log;

    @Autowired
    public BackupRestController(ICustomerFacade customerFacade, Log Log) {
        this.customerFacade = customerFacade;
        this.Log = Log;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/export")
    public @ResponseBody
    ResponseEntity<RespondDTO<byte[]>> exportBackup() {
        RespondDTO<byte[]> respondDTO = new RespondDTO<>();
        respondDTO.setT(customerFacade.exportBackUp());
        respondDTO.setMessage("Done");

        return new ResponseEntity<>(respondDTO, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/import")
    public @ResponseBody
    ResponseEntity<RespondDTO> importBackup(@RequestBody byte[] bytes) {
        RespondDTO respondDTO = new RespondDTO();
        customerFacade.removeAll();
        customerFacade.importBackUp(bytes);
        respondDTO.setMessage("Done");

        return new ResponseEntity<>(respondDTO, HttpStatus.OK);
    }
}
