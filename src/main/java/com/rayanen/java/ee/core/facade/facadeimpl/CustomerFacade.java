package com.rayanen.java.ee.core.facade.facadeimpl;

import com.rayanen.java.ee.core.controller.ICustomerService;
import com.rayanen.java.ee.core.dto.CustomerDTO;
import com.rayanen.java.ee.core.dto.ReportType;
import com.rayanen.java.ee.core.dto.UserPassDTO;
import com.rayanen.java.ee.core.exceptions.*;
import com.rayanen.java.ee.core.facade.ICustomerFacade;
import com.rayanen.java.ee.core.facade.conversion.Convert;
import com.rayanen.java.ee.core.log.Log;
import com.rayanen.java.ee.core.model.entity.CustomerEntity;
import com.rayanen.java.ee.core.model.entity.UserPassEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


@Component
@Transactional
public class CustomerFacade implements ICustomerFacade {

    private final ICustomerService customerServise;

    private final Convert convert;

    private final Log Log;

    @Autowired
    public CustomerFacade(ICustomerService customerServise, Convert convert, Log Log) {
        this.customerServise = customerServise;
        this.convert = convert;
        this.Log = Log;
    }

    @Override
    public void insert(CustomerDTO customerDTO) throws StoreFailedException, CustomerIDDuplicateException, EmailNotValidException, CustomerIDNotValidExeption, LastNameNotValidException, NameNotValidException {
        Log.setInfoLog("Start");
        CustomerEntity customerEntity = convert.DtoToEntity(customerDTO);
        customerServise.insert(customerEntity);
        Log.setInfoLog("End");
    }

    @Override
    public CustomerDTO searchByCustomerID(String customerID) throws CustomerIDNotFoundException {
        Log.setInfoLog("Start");
        return convert.EntityToDto(customerServise.searchByCustomerID(customerID));
    }

    @Override
    public void editName(CustomerDTO customerDTO, String name) throws CustomerIDNotFoundException {
        Log.setInfoLog("Start");
        customerServise.editName(convert.DtoToEntity(customerDTO), name);
        Log.setInfoLog("End");
    }

    @Override
    public void editLastName(CustomerDTO customerDTO, String lastName) throws CustomerIDNotFoundException {
        Log.setInfoLog("Start");
        customerServise.editLastName(convert.DtoToEntity(customerDTO), lastName);
        Log.setInfoLog("End");
    }

    @Override
    public void remove(String customerID) throws CustomerIDNotFoundException {
        Log.setInfoLog("Start");
        customerServise.remove(customerID);
        Log.setInfoLog("End");
    }

    @Override
    public CustomerDTO getCustomerByDepositNumber(String depositNumber) throws DepositNumberNotFoundException {
        Log.setInfoLog("Start");
        CustomerEntity customerEntity = customerServise.getCustomerByDepositNumber(depositNumber);
        return convert.EntityToDto(customerEntity);
    }

    @Override
    public byte[] exportBackUp() {
        Log.setInfoLog("Start");
        return customerServise.exportBackUp();
    }

    @Override
    public void removeAll() {
        customerServise.removeAll();
    }

    @Override
    public void importBackUp(byte[] bytes) {
        Log.setInfoLog("Start");
        customerServise.importBackUp(bytes);
        Log.setInfoLog("End");
    }

    @Override
    public byte[] getCustomerReport(ReportType reportType) {
        return customerServise.getCustomerReport(reportType);
    }

    @Override
    public byte[] getDepositForACustomerReport(String name, ReportType reportType) throws CustomerNameNotFoundException {
        return customerServise.getDepositForACustomerReport(name, reportType);
    }

    @Override
    public boolean checkUserPass(UserPassDTO userPassDTO) {
        return customerServise.checkUserPass(new UserPassEntity(userPassDTO.getUsername(), userPassDTO.getPassword()));
    }

    @Override
    public void insertUser(String username, String password, String roleName) {
        customerServise.insertUser(username, password, roleName);
    }
}
