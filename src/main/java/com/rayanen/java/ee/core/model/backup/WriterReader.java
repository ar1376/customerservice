package com.rayanen.java.ee.core.model.backup;

import com.rayanen.java.ee.core.log.Log;
import com.rayanen.java.ee.core.model.entity.CustomerEntity;
import com.rayanen.java.ee.core.model.entity.DepositEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Component
public class WriterReader {

    private final Log Log;

    @Autowired
    public WriterReader(Log Log) {
        this.Log = Log;
    }

    public void writeFile(List<CustomerEntity> save) {
        Log.setInfoLog("Start");
        try {
            FileOutputStream f = new FileOutputStream(new File("Banking.backup"));
            ObjectOutputStream o = new ObjectOutputStream(f);

            // Write objects to file
            o.writeObject(save);

            o.close();
            f.close();
        } catch (IOException e) {
            System.out.println("Error initializing stream");
            Log.setWarnLog(e.getStackTrace().toString());
        }
        Log.setInfoLog("End");
    }

    public void writeFileA(List<DepositEntity> save) {
        Log.setInfoLog("Start");
        try {
            FileOutputStream f = new FileOutputStream(new File("Account"));
            ObjectOutputStream o = new ObjectOutputStream(f);

            // Write objects to file
            o.writeObject(save);

            o.close();
            f.close();
        } catch (IOException e) {
            System.out.println("Error initializing stream");
            Log.setWarnLog(e.getStackTrace().toString());
        }
        Log.setInfoLog("End");
    }

    public List<CustomerEntity> readFile() {
        Log.setInfoLog("Start");
        List<CustomerEntity> save = null;
        try {
            FileInputStream fi = new FileInputStream(new File("Banking.backup"));
            ObjectInputStream oi = new ObjectInputStream(fi);

            // Read objects
            save = (ArrayList<CustomerEntity>) oi.readObject();

            oi.close();
            fi.close();

        } catch (IOException e) {
            Log.setWarnLog(e.getStackTrace().toString());
            System.out.println("Error initializing stream");
        } catch (ClassNotFoundException e) {
            Log.setWarnLog(e.getStackTrace().toString());
        }
        return save;

    }

    public List<DepositEntity> readFileA() {
        Log.setInfoLog("Start");
        List<DepositEntity> save = null;
        try {
            FileInputStream fi = new FileInputStream(new File("Account"));
            ObjectInputStream oi = new ObjectInputStream(fi);

            // Read objects
            save = (List<DepositEntity>) oi.readObject();

            oi.close();
            fi.close();

        } catch (IOException e) {
            Log.setWarnLog(e.getStackTrace().toString());
            System.out.println("Error initializing stream");
        } catch (ClassNotFoundException e) {
            Log.setWarnLog(e.getStackTrace().toString());
        }
        return save;
    }
}
