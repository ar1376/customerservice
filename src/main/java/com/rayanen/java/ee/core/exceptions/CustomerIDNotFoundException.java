package com.rayanen.java.ee.core.exceptions;

public class CustomerIDNotFoundException extends Exception {
    @Override
    public String getMessage() {
        return "Wrong Customer ID !";
    }
}
