package com.rayanen.java.ee.core.exceptions;

public class CustomerIDNotValidExeption extends Exception {
    @Override
    public String getMessage() {
        return "Customer ID Should Be 8 Digit !";
    }
}
