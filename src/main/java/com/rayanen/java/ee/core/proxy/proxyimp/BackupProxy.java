package com.rayanen.java.ee.core.proxy.proxyimp;

import com.rayanen.java.ee.core.dto.RespondDTO;
import com.rayanen.java.ee.core.proxy.IBackupProxy;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.faces.context.FacesContext;

@Component
public class BackupProxy implements IBackupProxy{

    @Override
    public RespondDTO<byte[]> exportBackup() {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders requestHeaders = new HttpHeaders();

        String headerValue = FacesContext.getCurrentInstance().getExternalContext().getRequestHeaderMap().get("cookie");
        requestHeaders.add("Cookie", headerValue);

        HttpEntity requestEntity = new HttpEntity(requestHeaders);

        ResponseEntity<RespondDTO<byte[]>> response = restTemplate.exchange(
                "http://localhost:8080/ws/backup/export",
                HttpMethod.POST,
                requestEntity,
                new ParameterizedTypeReference<RespondDTO<byte[]>>(){}
        );
        return response.getBody();
    }

    @Override
    public RespondDTO importBackup(byte[] bytes) {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders requestHeaders = new HttpHeaders();

        String headerValue = FacesContext.getCurrentInstance().getExternalContext().getRequestHeaderMap().get("cookie");
        requestHeaders.add("Cookie", headerValue);

        HttpEntity<byte[]> requestEntity = new HttpEntity<>(bytes,requestHeaders);

        ResponseEntity<RespondDTO> response = restTemplate.exchange(
                "http://localhost:8080/ws/backup/export",
                HttpMethod.POST,
                requestEntity,
                RespondDTO.class
        );
        return response.getBody();
    }
}
