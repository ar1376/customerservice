package com.rayanen.java.ee.core.facade;


import com.rayanen.java.ee.core.dto.CustomerDTO;
import com.rayanen.java.ee.core.dto.ReportType;
import com.rayanen.java.ee.core.dto.UserPassDTO;
import com.rayanen.java.ee.core.exceptions.*;
import org.springframework.security.access.prepost.PreAuthorize;

public interface ICustomerFacade {
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    void insert(CustomerDTO customerDTO) throws StoreFailedException, CustomerIDDuplicateException, EmailNotValidException, CustomerIDNotValidExeption, LastNameNotValidException, NameNotValidException;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    CustomerDTO searchByCustomerID(String customerID) throws CustomerIDNotFoundException;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    void editName(CustomerDTO customerDTO, String name) throws CustomerIDNotFoundException;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    void editLastName(CustomerDTO customerDTO, String lastName) throws CustomerIDNotFoundException;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    void remove(String customerID) throws CustomerIDNotFoundException;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    CustomerDTO getCustomerByDepositNumber(String depositNumber) throws DepositNumberNotFoundException;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    byte[] exportBackUp();

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    void removeAll();

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    void importBackUp(byte[] bytes);

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    byte[] getCustomerReport(ReportType reportType);

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    byte[] getDepositForACustomerReport(String name, ReportType reportType) throws CustomerNameNotFoundException;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    boolean checkUserPass(UserPassDTO userPassDTO);

    void insertUser(String username, String password, String roleName);
}
