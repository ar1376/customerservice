package com.rayanen.java.ee.core.proxy;

import com.rayanen.java.ee.core.dto.CustomerDTO;
import com.rayanen.java.ee.core.dto.RespondDTO;
import org.springframework.http.ResponseEntity;

public interface ICustomerProxy {
    RespondDTO insertCustomer(CustomerDTO customerDTO);

    RespondDTO updateCustomer(CustomerDTO customerDTO);

    RespondDTO deleteCustomer(CustomerDTO customerDTO);
}
