package com.rayanen.java.ee.core.ui.benefitbean;

import com.rayanen.java.ee.core.dto.RespondDTO;
import com.rayanen.java.ee.core.proxy.IBenefitProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

@Component
@Scope("view")
public class BenefitBean {

    final
    IBenefitProxy benefitProxy;

    @Autowired
    public BenefitBean(IBenefitProxy benefitProxy) {
        this.benefitProxy = benefitProxy;
    }

    public void getBenefit(ActionEvent actionEvent) {
        RespondDTO response = benefitProxy.getBenefit();
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, response.getMessage(), ""));
    }
}
