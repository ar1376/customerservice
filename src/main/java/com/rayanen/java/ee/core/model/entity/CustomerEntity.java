package com.rayanen.java.ee.core.model.entity;

import com.google.gson.annotations.Expose;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "CUSTOMER")
public class CustomerEntity {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private int ID;

    @Expose
    @Column(name = "NAME")
    private String name;

    @Expose
    @Column(name = "LASTNAME")
    private String lastName;

    @Expose
    @Column(name = "EMAIL")
    private String email;

    @Expose
    @Column(name = "CUSTOMERID")
    private String customerID;

    @Expose
    @OneToOne(cascade = {CascadeType.ALL})
    private UserPassEntity userPassEntity;

    @Expose
    @OneToMany(cascade = {CascadeType.ALL}, orphanRemoval = true)
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinColumn(name = "DP_ID")
    private List<DepositEntity> depositEntities;

    public CustomerEntity() {
    }

    public CustomerEntity(String name, String lastName, String email, String customerID) {
        this.name = name;
        this.lastName = lastName;
        this.email = email;
        this.customerID = customerID;
    }

    public CustomerEntity(String name, String lastName, String email, String customerID, List<DepositEntity> depositEntities) {
        this.name = name;
        this.lastName = lastName;
        this.email = email;
        this.customerID = customerID;
        this.depositEntities = depositEntities;
    }

    public CustomerEntity(String name, String lastName, String email, String customerID, UserPassEntity userPassEntity, List<DepositEntity> depositEntities) {
        this.name = name;
        this.lastName = lastName;
        this.email = email;
        this.customerID = customerID;
        this.userPassEntity = userPassEntity;
        this.depositEntities = depositEntities;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    public UserPassEntity getUserPassEntity() {
        return userPassEntity;
    }

    public void setUserPassEntity(UserPassEntity userPassEntity) {
        this.userPassEntity = userPassEntity;
    }

    public List<DepositEntity> getDepositEntities() {
        return depositEntities;
    }

    public void setDepositEntities(List<DepositEntity> depositEntities) {
        this.depositEntities = depositEntities;
    }
}
