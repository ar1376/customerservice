package com.rayanen.java.ee.core.ui.depositbean;

import com.rayanen.java.ee.core.dto.RespondDTO;
import com.rayanen.java.ee.core.proxy.IDepositProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import java.math.BigDecimal;

@Component
@Scope("view")
public class DepositWithdrawBean {
    private BigDecimal cash;
    private String depositNumber;
    private String getCashString;

    private final IDepositProxy depositProxy;

    @Autowired
    public DepositWithdrawBean(IDepositProxy depositProxy) {
        this.depositProxy = depositProxy;
    }

    public BigDecimal getCash() {
        return cash;
    }

    public void setCash(BigDecimal cash) {
        this.cash = cash;
    }

    public String getDepositNumber() {
        return depositNumber;
    }

    public void setDepositNumber(String depositNumber) {
        this.depositNumber = depositNumber;
    }

    public String getGetCashString() {
        return getCashString;
    }

    public void setGetCashString(String getCashString) {
        this.getCashString = getCashString;
    }

    public void withdraw(ActionEvent actionEvent) {
        RespondDTO response = depositProxy.withdrawDeposit(cash = new BigDecimal(Long.parseLong(getCashString)), depositNumber);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, response.getMessage(), ""));
    }
}
