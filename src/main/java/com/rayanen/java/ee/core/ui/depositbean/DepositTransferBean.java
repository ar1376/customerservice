package com.rayanen.java.ee.core.ui.depositbean;

import com.rayanen.java.ee.core.dto.RespondDTO;
import com.rayanen.java.ee.core.proxy.IDepositProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import java.math.BigDecimal;

@Component
@Scope("view")
public class DepositTransferBean {
    private BigDecimal cash;
    private String depositNumberSender;
    private String depositNumberReceiver;
    private String getCashString;

    private final IDepositProxy depositProxy;

    @Autowired
    public DepositTransferBean(IDepositProxy depositProxy) {
        this.depositProxy = depositProxy;
    }

    public BigDecimal getCash() {
        return cash;
    }

    public void setCash(BigDecimal cash) {
        this.cash = cash;
    }

    public String getDepositNumberSender() {
        return depositNumberSender;
    }

    public void setDepositNumberSender(String depositNumberSender) {
        this.depositNumberSender = depositNumberSender;
    }

    public String getDepositNumberReceiver() {
        return depositNumberReceiver;
    }

    public void setDepositNumberReceiver(String depositNumberReceiver) {
        this.depositNumberReceiver = depositNumberReceiver;
    }

    public String getGetCashString() {
        return getCashString;
    }

    public void setGetCashString(String getCashString) {
        this.getCashString = getCashString;
    }

    public void transfer(ActionEvent actionEvent) {
        RespondDTO response = depositProxy.transferDeposit(depositNumberSender, depositNumberReceiver, cash = new BigDecimal(Long.parseLong(getCashString)));
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, response.getMessage(), ""));
    }
}
