package com.rayanen.java.ee.core.dto;

public enum DepositType {
    SHORT_TERM, LONG_TERM;

    public static DepositType asMyEnum(String str) {
        for (DepositType me : DepositType.values()) {
            if (me.name().equalsIgnoreCase(str))
                return me;
        }
        return null;
    }

}
