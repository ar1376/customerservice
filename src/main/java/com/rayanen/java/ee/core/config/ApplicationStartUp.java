package com.rayanen.java.ee.core.config;

import com.rayanen.java.ee.core.facade.ICustomerFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;


@Component
public class ApplicationStartUp implements ApplicationListener<ContextRefreshedEvent> {

    private final ICustomerFacade customerFacade;

    @Autowired
    public ApplicationStartUp(ICustomerFacade customerFacade) {
        this.customerFacade = customerFacade;
    }

    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        customerFacade.insertUser("admin", "123456", "ROLE_ADMIN");
        customerFacade.insertUser("user", "123456", "ROLE_USER");
    }
}
