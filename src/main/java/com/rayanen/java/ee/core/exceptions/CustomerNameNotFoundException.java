package com.rayanen.java.ee.core.exceptions;

public class CustomerNameNotFoundException extends Exception {
    @Override
    public String getMessage() {
        return "Name Not Found !";
    }
}
