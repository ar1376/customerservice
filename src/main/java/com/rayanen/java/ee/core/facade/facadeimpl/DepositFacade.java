package com.rayanen.java.ee.core.facade.facadeimpl;

import com.rayanen.java.ee.core.controller.serviceimpl.DepositService;
import com.rayanen.java.ee.core.dto.DepositDTO;
import com.rayanen.java.ee.core.dto.ReportType;
import com.rayanen.java.ee.core.exceptions.*;
import com.rayanen.java.ee.core.facade.IDepositFacade;
import com.rayanen.java.ee.core.facade.conversion.Convert;
import com.rayanen.java.ee.core.log.Log;
import com.rayanen.java.ee.core.model.entity.DepositEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

@Component
@Transactional
public class DepositFacade implements IDepositFacade {

    private final DepositService depositService;

    private final Convert convert;

    private final Log Log;

    @Autowired
    public DepositFacade(DepositService depositService, Convert convert, Log Log) {
        this.depositService = depositService;
        this.convert = convert;
        this.Log = Log;
    }

    @Override
    public void insert(String customerID, DepositDTO depositDTO) throws DepositNumberNotValidException, CustomerIDNotFoundException, DepositNumberDuplicateException {
        Log.setInfoLog("Start");
        depositService.insert(customerID, convert.AccountDtoToEntity(depositDTO));
        Log.setInfoLog("End");
    }

    @Override
    public void editDepositNumber(String depositNumber, String newDepositNumber) throws DepositNumberNotFoundException, DepositNumberNotValidException, DepositNumberDuplicateException {
        Log.setInfoLog("Start");
        depositService.editDepositNumber(depositNumber, newDepositNumber);
        Log.setInfoLog("End");
    }

    @Override
    public void remove(String depositNumber) throws DepositNumberNotFoundException {
        Log.setInfoLog("Start");
        depositService.remove(depositNumber);
        Log.setInfoLog("End");
    }

    @Override
    public void deposit(BigDecimal cash, String depositNumber) throws CashNotValidException, DepositNumberNotFoundException {
        Log.setInfoLog("Start");
        depositService.deposit(cash, depositNumber);
        Log.setInfoLog("End");
    }

    @Override
    public void withdraw(BigDecimal cash, String depositNumber) throws CashNotValidException, NotEnoughCashException, DepositNumberNotFoundException {
        Log.setInfoLog("Start");
        depositService.withdraw(cash, depositNumber);
        Log.setInfoLog("End");
    }

    @Override
    public void transfer(BigDecimal cash, String depositNumberSender, String depositNumberReceiver) throws SenderWrongDepositNumberException, NotEnoughCashException, ReceiverWrongDepositNumberException, CashNotValidException {
        Log.setInfoLog("Start");
        depositService.transfer(cash, depositNumberSender, depositNumberReceiver);
        Log.setInfoLog("End");
    }

    @Override
    public DepositDTO searchByDepositNumber(String depositNumber) throws DepositNumberNotFoundException {
        Log.setInfoLog("Start");
        DepositEntity depositEntity = depositService.searchByDepositNumber(depositNumber);
        return convert.AccountEntityToDto(depositEntity);
    }

    @Override
    public byte[] getDepositReport(ReportType reportType){
        return depositService.getDepositReport(reportType);
    }

    @Override
    public void getBenefit(){
        depositService.getBenefit();
    }
}
