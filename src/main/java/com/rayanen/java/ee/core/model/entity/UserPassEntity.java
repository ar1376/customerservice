package com.rayanen.java.ee.core.model.entity;

import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "USERPASS")
public class UserPassEntity {

    @Expose
    @Id
    @Column(name = "USERNAME")
    private String username;

    @Expose
    @Column(name = "PASSWORD")
    private String password;

    @Expose
    @Column(name = "enabled")
    private boolean enabled;

    @Expose
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<UserRole> userRole = new HashSet<UserRole>(0);

    public UserPassEntity() {
    }

    public UserPassEntity(String username, String password, boolean enabled, Set<UserRole> userRole) {
        this.username = username;
        this.password = password;
        this.enabled = enabled;
        this.userRole = userRole;
    }

    public UserPassEntity(String username, String password, Set<UserRole> userRole) {
        this.username = username;
        this.password = password;
        this.userRole = userRole;
    }

    public UserPassEntity(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Set<UserRole> getUserRole() {
        return userRole;
    }

    public void setUserRole(Set<UserRole> userRole) {
        this.userRole = userRole;
    }
}
