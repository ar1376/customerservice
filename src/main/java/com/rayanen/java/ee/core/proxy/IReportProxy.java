package com.rayanen.java.ee.core.proxy;

import com.rayanen.java.ee.core.dto.ReportType;
import com.rayanen.java.ee.core.dto.RespondDTO;

public interface IReportProxy {

    RespondDTO<byte[]> customerReport(ReportType reportType);

    RespondDTO<byte[]> depositsReport(ReportType reportType);

    RespondDTO<byte[]> depositForACustomer(String name, ReportType reportType);
}
