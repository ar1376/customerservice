package com.rayanen.java.ee.core.dto;

import java.math.BigDecimal;
import java.util.List;

public class DepositDTO {

    private BigDecimal cash;
    private String depositNumber;
    private DepositType depositType;
    private List<LoackerDTO> loackerDTOList;

    public DepositDTO() {
    }

    public DepositDTO(String depositNumber, DepositType depositType) {
        this.depositNumber = depositNumber;
        this.depositType = depositType;
    }

    public DepositDTO(BigDecimal cash, String depositNumber, DepositType depositType) {
        this.cash = cash;
        this.depositNumber = depositNumber;
        this.depositType = depositType;
    }

    public BigDecimal getCash() {
        return cash;
    }

    public void setCash(BigDecimal cash) {
        this.cash = cash;
    }

    public String getDepositNumber() {
        return depositNumber;
    }

    public void setDepositNumber(String depositNumber) {
        this.depositNumber = depositNumber;
    }

    public List<LoackerDTO> getLoackerDTOList() {
        return loackerDTOList;
    }

    public void setLoackerDTOList(List<LoackerDTO> loackerDTOList) {
        this.loackerDTOList = loackerDTOList;
    }

    public DepositType getDepositType() {
        return depositType;
    }

    public void setDepositType(DepositType depositType) {
        this.depositType = depositType;
    }
}
