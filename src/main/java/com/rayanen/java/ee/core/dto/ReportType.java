package com.rayanen.java.ee.core.dto;

public enum ReportType {
    PDF,EXCEL;

    public static ReportType asMyEnum(String str) {
        for (ReportType me : ReportType.values()) {
            if (me.name().equalsIgnoreCase(str))
                return me;
        }
        return null;
    }
}
