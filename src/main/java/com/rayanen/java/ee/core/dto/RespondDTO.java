package com.rayanen.java.ee.core.dto;

public class RespondDTO<T> {
    private T t;
    private String message;

    public RespondDTO() {
    }

    public RespondDTO(T t, String message) {
        this.t = t;
        this.message = message;
    }

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
