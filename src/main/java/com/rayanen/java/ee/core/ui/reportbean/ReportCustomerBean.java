package com.rayanen.java.ee.core.ui.reportbean;

import com.rayanen.java.ee.core.dto.ReportType;
import com.rayanen.java.ee.core.dto.RespondDTO;
import com.rayanen.java.ee.core.proxy.IReportProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Scope("view")
public class ReportCustomerBean {

    private String reportType;

    @Autowired
    public ReportCustomerBean(IReportProxy reportProxy) {
        this.reportProxy = reportProxy;
    }

    public String getReportType() {
        return reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }


    private final IReportProxy reportProxy;

    public void getCustomerReport(ActionEvent actionEvent){
        RespondDTO<byte[]> respondDTO = reportProxy.customerReport(ReportType.asMyEnum(reportType));
        byte[] bytes = respondDTO.getT();
        if (bytes != null) {
            HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            if (reportType.equals("PDF"))
                httpServletResponse.addHeader("Content-disposition", "attachment; filename=CustomerReport.pdf");
            else
                httpServletResponse.addHeader("Content-disposition", "attachment; filename=CustomerReport.xlsx");
            ServletOutputStream servletStream = null;
            try {
                servletStream = httpServletResponse.getOutputStream();
                servletStream.write(bytes);
            } catch (IOException e) {
                e.printStackTrace();
            }
            FacesContext.getCurrentInstance().responseComplete();
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, respondDTO.getMessage(), ""));
    }
}
