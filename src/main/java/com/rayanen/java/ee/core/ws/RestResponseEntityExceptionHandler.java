package com.rayanen.java.ee.core.ws;

import com.rayanen.java.ee.core.dto.RespondDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = { Throwable.class })
    protected ResponseEntity<RespondDTO> handleConflict(Throwable t, WebRequest request) {
        RespondDTO response = new RespondDTO();
        if(t instanceof AccessDeniedException){
            response.setMessage("Access Denied");
        }else{
            response.setMessage("System Error");
        }
        return new ResponseEntity<RespondDTO>(response, HttpStatus.OK);
    }

}
