package com.rayanen.java.ee.core.exceptions;

public class SenderWrongDepositNumberException extends Exception {
    @Override
    public String getMessage() {
        return "Wrong Deposit Number For Sender !";
    }
}
