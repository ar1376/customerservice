package com.rayanen.java.ee.core.ui.depositbean;

import com.rayanen.java.ee.core.dto.DepositDTO;
import com.rayanen.java.ee.core.dto.DepositType;
import com.rayanen.java.ee.core.dto.RespondDTO;
import com.rayanen.java.ee.core.proxy.IDepositProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

@Component
@Scope("view")
public class DepositInsertBean {
    private DepositDTO depositDTO = new DepositDTO();
    private String customerID;
    private String type;

    private final IDepositProxy depositProxy;

    @Autowired
    public DepositInsertBean(IDepositProxy depositProxy) {
        this.depositProxy = depositProxy;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public DepositDTO getDepositDTO() {
        return depositDTO;
    }

    public void setDepositDTO(DepositDTO depositDTO) {
        this.depositDTO = depositDTO;
    }

    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    public void insert(ActionEvent actionEvent) {
        depositDTO.setDepositType(DepositType.asMyEnum(type));
        RespondDTO response = depositProxy.insertDeposit(customerID, depositDTO);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, response.getMessage(), ""));
    }
}
