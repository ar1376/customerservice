package com.rayanen.java.ee.core.ui.depositbean;

import com.rayanen.java.ee.core.dto.RespondDTO;
import com.rayanen.java.ee.core.proxy.IDepositProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

@Component
@Scope("view")
public class DepositUpdateBean {
    private String depositNumber;
    private String newDepositNumber;

    private final IDepositProxy depositProxy;

    @Autowired
    public DepositUpdateBean(IDepositProxy depositProxy) {
        this.depositProxy = depositProxy;
    }

    public String getDepositNumber() {
        return depositNumber;
    }

    public void setDepositNumber(String depositNumber) {
        this.depositNumber = depositNumber;
    }

    public String getNewDepositNumber() {
        return newDepositNumber;
    }

    public void setNewDepositNumber(String newDepositNumber) {
        this.newDepositNumber = newDepositNumber;
    }

    public void update(ActionEvent actionEvent) {
        RespondDTO response = depositProxy.updateDeposit(depositNumber, newDepositNumber);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, response.getMessage(), ""));
    }
}
