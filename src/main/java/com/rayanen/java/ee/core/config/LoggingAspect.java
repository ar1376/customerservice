package com.rayanen.java.ee.core.config;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggingAspect {


    @Before("within(com.rayanen.java.ee.core.facade..*) || within(com.rayanen.java.ee.core.model..*) || within(com.rayanen.java.ee.core.controller..*) || within(com.rayanen.java.ee.core.ui..*)")
    public void logBefore(JoinPoint joinPoint) {
        System.out.println("logBefore() is running!");
        System.out.println("name : " + joinPoint.getSignature().getName());
        System.out.println("******");
    }

    @After("within(com.rayanen.java.ee.core..*)")
    public void logAfter(JoinPoint joinPoint) {
        System.out.println("logAfter() is running!");
        System.out.println("name : " + joinPoint.getSignature().getName());
        System.out.println("******");

    }
}
