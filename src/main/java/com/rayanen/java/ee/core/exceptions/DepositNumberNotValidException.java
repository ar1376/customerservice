package com.rayanen.java.ee.core.exceptions;

public class DepositNumberNotValidException extends  Exception {
    @Override
    public String getMessage() {
        return "Deposit Number Should Be 10 Digit !";
    }
}
