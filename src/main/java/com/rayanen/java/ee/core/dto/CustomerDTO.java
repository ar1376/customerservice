package com.rayanen.java.ee.core.dto;

import java.util.List;

public class CustomerDTO {
    private String name;
    private String lastName;
    private String email;
    private String customerID;
    private UserPassDTO userPassDTO = new UserPassDTO();
    private List<DepositDTO> depositDTOS;

    public CustomerDTO() {
    }

    public CustomerDTO(String name, String lastName, String email, String customerID, UserPassDTO userPassDTO) {
        this.name = name;
        this.lastName = lastName;
        this.email = email;
        this.customerID = customerID;
        this.userPassDTO = userPassDTO;
    }

    public CustomerDTO(String name, String lastName, String email, String customerID, List<DepositDTO> depositDTOS) {
        this.name = name;
        this.lastName = lastName;
        this.email = email;
        this.customerID = customerID;
        this.depositDTOS = depositDTOS;
    }

    public CustomerDTO(String name, String lastName, String email, String customerID, UserPassDTO userPassDTO, List<DepositDTO> depositDTOS) {
        this.name = name;
        this.lastName = lastName;
        this.email = email;
        this.customerID = customerID;
        this.userPassDTO = userPassDTO;
        this.depositDTOS = depositDTOS;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    public UserPassDTO getUserPassDTO() {
        return userPassDTO;
    }

    public void setUserPassDTO(UserPassDTO userPassDTO) {
        this.userPassDTO = userPassDTO;
    }    public List<DepositDTO> getDepositDTOS() {
        return depositDTOS;
    }

    public void setDepositDTOS(List<DepositDTO> depositDTOS) {
        this.depositDTOS = depositDTOS;
    }
}
