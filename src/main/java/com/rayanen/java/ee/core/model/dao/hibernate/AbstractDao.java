package com.rayanen.java.ee.core.model.dao.hibernate;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by d.jalalnouri on 11/16/2017.
 */
public abstract class AbstractDao {

    @PersistenceContext
    protected EntityManager em;

}
