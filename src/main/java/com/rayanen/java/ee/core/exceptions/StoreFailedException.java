package com.rayanen.java.ee.core.exceptions;

public class StoreFailedException extends Exception {
    @Override
    public String getMessage() {
        return "Can't Store Data";
    }
}
