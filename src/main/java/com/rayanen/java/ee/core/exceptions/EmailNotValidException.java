package com.rayanen.java.ee.core.exceptions;

public class EmailNotValidException extends Exception {
    @Override
    public String getMessage() {
        return "Email Was Not Valid !";
    }
}
