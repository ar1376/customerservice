package com.rayanen.java.ee.core.exceptions;

public class CashNotValidException extends Exception {

    @Override
    public String getMessage() {
        return "Money Amount Was Not Valid !";
    }
}
