package com.rayanen.java.ee.core.model.entity;

import com.google.gson.annotations.Expose;
import com.rayanen.java.ee.core.dto.DepositType;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "DEPOSIT")
public class DepositEntity {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private int ID;

    @Expose
    @Column(name = "CASH")
    private BigDecimal cash;

    @Expose
    @Column(name = "DEPOSITNUMBER", unique = true)
    private String depositNumber;

    @Expose
    @Column(name = "DEPOSITTYPE")
    @Enumerated(EnumType.STRING)
    private DepositType depositType;

    @Expose
    @Column(name = "LASTBENEFIT")
    private Date lastBenefit;

    @Expose
    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "LOACKERJOINID")
    private List<LoackerEntity> loackerEntity = new ArrayList<>();

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "CUSTOMERJOINID")
    private CustomerEntity customerEntity;

    public DepositEntity() {
    }

    public DepositEntity(BigDecimal cash, String depositNumber, DepositType depositType) {
        this.cash = cash;
        this.depositNumber = depositNumber;
        this.depositType = depositType;
        this.lastBenefit = new Date();
    }

    public Date getLastBenefit() {
        return lastBenefit;
    }

    public void setLastBenefit(Date lastBenefit) {
        this.lastBenefit = lastBenefit;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public BigDecimal getCash() {
        return cash;
    }

    public void setCash(BigDecimal cash) {
        this.cash = cash;
    }

    public String getDepositNumber() {
        return depositNumber;
    }

    public void setDepositNumber(String depositNumber) {
        this.depositNumber = depositNumber;
    }

    public List<LoackerEntity> getLoackerEntity() {
        return loackerEntity;
    }

    public void setLoackerEntity(List<LoackerEntity> loackerEntity) {
        this.loackerEntity = loackerEntity;
    }

    public DepositType getDepositType() {
        return depositType;
    }

    public void setDepositType(DepositType depositType) {
        this.depositType = depositType;
    }

    public CustomerEntity getCustomerEntity() {
        return customerEntity;
    }

    public void setCustomerEntity(CustomerEntity customerEntity) {
        this.customerEntity = customerEntity;
    }
}
