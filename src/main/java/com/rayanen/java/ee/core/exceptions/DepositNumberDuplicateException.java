package com.rayanen.java.ee.core.exceptions;

public class DepositNumberDuplicateException extends Exception {
    @Override
    public String getMessage() {
        return "Deposit Number Already Exist !";
    }
}
