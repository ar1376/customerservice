package com.rayanen.java.ee.core.report;

import net.sf.jasperreports.engine.JRDataSource;

public abstract class Report {

    public Report() {
    }

    public abstract byte[] getReport(JRDataSource jrDataSource);

    public abstract byte[] getExcelReport(JRDataSource jrDataSource);
}
