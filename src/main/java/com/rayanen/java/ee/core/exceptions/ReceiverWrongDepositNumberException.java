package com.rayanen.java.ee.core.exceptions;

public class ReceiverWrongDepositNumberException extends Exception {
    @Override
    public String getMessage() {
        return "Wrong Deposit Number For Receiver !";
    }
}
