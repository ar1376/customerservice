package com.rayanen.java.ee.core.controller;


import com.rayanen.java.ee.core.dto.ReportType;
import com.rayanen.java.ee.core.exceptions.*;
import com.rayanen.java.ee.core.model.entity.CustomerEntity;
import com.rayanen.java.ee.core.model.entity.UserPassEntity;

public interface ICustomerService {
    void insert(CustomerEntity customerEntity) throws StoreFailedException, CustomerIDDuplicateException, EmailNotValidException, CustomerIDNotValidExeption, LastNameNotValidException, NameNotValidException;

    CustomerEntity searchByCustomerID(String customerID) throws CustomerIDNotFoundException;

    void editName(CustomerEntity customerEntity, String name) throws CustomerIDNotFoundException;

    void editLastName(CustomerEntity customerEntity, String lastName) throws CustomerIDNotFoundException;

    void remove(String customerID) throws CustomerIDNotFoundException;

    CustomerEntity getCustomerByDepositNumber(String depositNumber) throws DepositNumberNotFoundException;

    byte[] getCustomerReport(ReportType reportType);

    byte[] getDepositForACustomerReport(String name , ReportType reportType) throws CustomerNameNotFoundException;

    byte[] exportBackUp();

    void removeAll();

    void importBackUp(byte[] bytes);

    boolean checkUserPass(UserPassEntity userPassEntity);

    void insertUser(String username, String password, String roleName);
}
