package com.rayanen.java.ee.core.ui;

import org.springframework.context.annotation.Scope;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

@Component
@Scope("view")
public class MenuBean {
    public Boolean showMenu(String str) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        for (String s : str.split(",")) {
            if (user.getAuthorities().contains(new SimpleGrantedAuthority(s)))
                return true;
        }
        return false;
    }

    public String getUserName() {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return user.getUsername();
    }
}
