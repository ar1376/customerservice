package com.rayanen.java.ee.core.log;

import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

@Component
public class Log {
    private final static Logger mylog = Logger.getLogger("bankLogs");
    static private FileHandler fileTxt;

    static {
        try {
            fileTxt = new FileHandler("Logging.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setInfoLog(String log) {
        mylog.setUseParentHandlers(false);
        mylog.addHandler(fileTxt);
        mylog.info(log);
    }

    public void setWarnLog(String log) {
        mylog.setUseParentHandlers(false);
        mylog.addHandler(fileTxt);
        mylog.warning(log);
    }

}