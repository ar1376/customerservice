package com.rayanen.java.ee.core.ui.customerbean;

import com.rayanen.java.ee.core.dto.CustomerDTO;
import com.rayanen.java.ee.core.dto.RespondDTO;
import com.rayanen.java.ee.core.proxy.ICustomerProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

@Component
@Scope("view")
public class CustomerUpdateBean {

    private CustomerDTO customerDTO = new CustomerDTO();

    private final ICustomerProxy iCustomerProxy;

    @Autowired
    public CustomerUpdateBean(ICustomerProxy iCustomerProxy) {
        this.iCustomerProxy = iCustomerProxy;
    }

    public CustomerDTO getCustomerDTO() {
        return customerDTO;
    }

    public void setCustomerDTO(CustomerDTO customerDTO) {
        this.customerDTO = customerDTO;
    }

    public void update(ActionEvent actionEvent) {
        RespondDTO response = iCustomerProxy.updateCustomer(customerDTO);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, response.getMessage(), ""));
    }
}
