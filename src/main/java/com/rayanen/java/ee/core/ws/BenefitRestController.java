package com.rayanen.java.ee.core.ws;

import com.rayanen.java.ee.core.dto.RespondDTO;
import com.rayanen.java.ee.core.facade.IDepositFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/benefit")
public class BenefitRestController {

    private final IDepositFacade depositFacade;

    @Autowired
    public BenefitRestController(IDepositFacade depositFacade) {
        this.depositFacade = depositFacade;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/benefit")
    public @ResponseBody
    ResponseEntity<RespondDTO> getBenefit() {
        RespondDTO respondDTO = new RespondDTO();
        depositFacade.getBenefit();
        respondDTO.setMessage("Done");
        return new ResponseEntity<>(respondDTO,HttpStatus.OK);
    }
}
