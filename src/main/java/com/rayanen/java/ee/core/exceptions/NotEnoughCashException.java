package com.rayanen.java.ee.core.exceptions;

public class NotEnoughCashException extends Exception {
    @Override
    public String getMessage() {
        return "You Don't Have Enough Money !";
    }
}
