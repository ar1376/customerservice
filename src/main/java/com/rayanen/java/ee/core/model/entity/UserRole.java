package com.rayanen.java.ee.core.model.entity;

import com.google.gson.annotations.Expose;

import javax.persistence.*;

@Entity
@Table(name = "USER_ROLE")
public class UserRole {

    @Id
    @GeneratedValue
    @Column(name = "user_role_id")
    private int userRoleId;

    @ManyToOne
    @JoinColumn(name = "USERNAME")
    private UserPassEntity userPassEntity;

    @Expose
    @Column(name = "ROLE")
    private String role;

    public UserRole() {
    }

    public UserRole(String role) {
        this.role = role;
    }

    public int getUserRoleId() {
        return userRoleId;
    }

    public void setUserRoleId(int userRoleId) {
        this.userRoleId = userRoleId;
    }

    public UserPassEntity getUserPassEntity() {
        return userPassEntity;
    }

    public void setUserPassEntity(UserPassEntity userPassEntity) {
        this.userPassEntity = userPassEntity;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
