package com.rayanen.java.ee.core.proxy;

import com.rayanen.java.ee.core.dto.DepositDTO;
import com.rayanen.java.ee.core.dto.RespondDTO;
import com.rayanen.java.ee.core.ws.wrapper.ThreeWrapper;
import com.rayanen.java.ee.core.ws.wrapper.TwoWrapper;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;

public interface IDepositProxy {
    RespondDTO insertDeposit(String customerID , DepositDTO depositDTO);

    RespondDTO updateDeposit(String depositNumber , String newDepositNumber);

    RespondDTO deleteDeposit(String depositNumber);

    RespondDTO depositDeposit(BigDecimal cash , String depositNumber);

    RespondDTO withdrawDeposit(BigDecimal cash , String depositNumber);

    RespondDTO transferDeposit(String depositNumberSender, String depositNumberReceiver, BigDecimal cash);

}
