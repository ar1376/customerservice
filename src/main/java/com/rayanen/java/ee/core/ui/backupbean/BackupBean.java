package com.rayanen.java.ee.core.ui.backupbean;

import com.rayanen.java.ee.core.dto.RespondDTO;
import com.rayanen.java.ee.core.proxy.IBackupProxy;
import org.primefaces.event.FileUploadEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Scope("view")
public class BackupBean {

    private final IBackupProxy backupProxy;

    @Autowired
    public BackupBean(IBackupProxy backupProxy) {
        this.backupProxy = backupProxy;
    }

    public void exportBackup(ActionEvent actionEvent) {
        RespondDTO<byte[]> respondDTO = backupProxy.exportBackup();
        byte [] bytes = respondDTO.getT();
        HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        httpServletResponse.addHeader("Content-disposition", "attachment; filename=bank.json");
        ServletOutputStream servletStream;
        try {
            servletStream = httpServletResponse.getOutputStream();
            servletStream.write(bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
        FacesContext.getCurrentInstance().responseComplete();
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, respondDTO.getMessage(), ""));

    }

    public void importBackup(FileUploadEvent event) {
        RespondDTO respondDTO = backupProxy.importBackup(event.getFile().getContents());
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, respondDTO.getMessage(), ""));
    }
}
