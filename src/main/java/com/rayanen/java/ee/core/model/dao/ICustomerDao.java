package com.rayanen.java.ee.core.model.dao;

import com.rayanen.java.ee.core.exceptions.*;
import com.rayanen.java.ee.core.model.entity.CustomerEntity;
import com.rayanen.java.ee.core.model.entity.UserPassEntity;

import java.util.List;

public interface ICustomerDao {
    void insert(CustomerEntity customerEntity) throws StoreFailedException;

    CustomerEntity searchByCustomerID(String customerID) throws CustomerIDNotFoundException;

    boolean customerIDChecker(String customerID);

    void editName(String customerID, String name) throws CustomerIDNotFoundException;

    void editLastName(String customerID, String lastName) throws CustomerIDNotFoundException;

    void remove(String customerID) throws CustomerIDNotFoundException;

    List<CustomerEntity> getAll();

    CustomerEntity searchByCustomerName(String name) throws CustomerNameNotFoundException;

    List<CustomerEntity> exportBackUp();

    void removeAll();

    void importBackUp(List<CustomerEntity> customerEntityBackup);

    List<UserPassEntity> getAllUserPass();

    UserPassEntity findByUserName(String username);

    void insert(UserPassEntity userPassEntity);
}
