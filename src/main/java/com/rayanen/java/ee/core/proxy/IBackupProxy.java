package com.rayanen.java.ee.core.proxy;

import com.rayanen.java.ee.core.dto.RespondDTO;

public interface IBackupProxy {

    RespondDTO<byte[]> exportBackup();

    RespondDTO importBackup(byte[] bytes);
}
