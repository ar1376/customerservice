package com.rayanen.java.ee.core.controller;

import com.rayanen.java.ee.core.model.dao.ICustomerDao;
import com.rayanen.java.ee.core.model.entity.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service("userDetailsService")
public class MyUserDetailsService implements UserDetailsService {

    private final ICustomerDao customerDao;

    @Autowired
    public MyUserDetailsService(ICustomerDao customerDao) {
        this.customerDao = customerDao;
    }

    @Transactional(readOnly=true)
    public UserDetails loadUserByUsername(final String username)
            throws UsernameNotFoundException {

        com.rayanen.java.ee.core.model.entity.UserPassEntity userPassEntity = customerDao.findByUserName(username);
        List<GrantedAuthority> authorities =
                buildUserAuthority(userPassEntity.getUserRole());

        return buildUserForAuthentication(userPassEntity, authorities);

    }

    private User buildUserForAuthentication(com.rayanen.java.ee.core.model.entity.UserPassEntity userPassEntity,
                                                      List<GrantedAuthority> authorities) {
        return new User(userPassEntity.getUsername(), userPassEntity.getPassword(),
                userPassEntity.isEnabled(), true, true, true, authorities);
    }

    private List<GrantedAuthority> buildUserAuthority(Set<UserRole> userRoles) {

        Set<GrantedAuthority> setAuths = new HashSet<>();

        // Build user's authorities
        for (UserRole userRole : userRoles) {
            setAuths.add(new SimpleGrantedAuthority(userRole.getRole()));
        }

        List<GrantedAuthority> Result = new ArrayList<>(setAuths);

        return Result;
    }

}