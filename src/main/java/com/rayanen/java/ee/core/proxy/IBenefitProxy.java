package com.rayanen.java.ee.core.proxy;

import com.rayanen.java.ee.core.dto.RespondDTO;

public interface IBenefitProxy {

    RespondDTO getBenefit();
}
