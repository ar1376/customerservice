package com.rayanen.java.ee.core.proxy.proxyimp;

import com.rayanen.java.ee.core.dto.CustomerDTO;
import com.rayanen.java.ee.core.dto.RespondDTO;
import com.rayanen.java.ee.core.proxy.ICustomerProxy;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.faces.context.FacesContext;

@Component
public class CustomerProxy implements ICustomerProxy {

    @Override
    public RespondDTO insertCustomer(CustomerDTO customerDTO) {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders requestHeaders = new HttpHeaders();

        String headerValue = FacesContext.getCurrentInstance().getExternalContext().getRequestHeaderMap().get("cookie");
        requestHeaders.add("Cookie", headerValue);

        HttpEntity<CustomerDTO> requestEntity = new HttpEntity<>(customerDTO, requestHeaders);

        ResponseEntity<RespondDTO> response = restTemplate.exchange(
                "http://localhost:8080/ws/customer/insert",
                HttpMethod.POST,
                requestEntity,
                RespondDTO.class
        );
        return response.getBody();
    }

    @Override
    public RespondDTO updateCustomer(CustomerDTO customerDTO) {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders requestHeaders = new HttpHeaders();

        String headerValue = FacesContext.getCurrentInstance().getExternalContext().getRequestHeaderMap().get("cookie");
        requestHeaders.add("Cookie", headerValue);

        HttpEntity<CustomerDTO> requestEntity = new HttpEntity<>(customerDTO, requestHeaders);
        ResponseEntity<RespondDTO> response = restTemplate.exchange(
                "http://localhost:8080/ws/customer/update",
                HttpMethod.POST,
                requestEntity,
                RespondDTO.class
        );
        return response.getBody();
    }

    @Override
    public RespondDTO deleteCustomer(CustomerDTO customerDTO) {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders requestHeaders = new HttpHeaders();

        String headerValue = FacesContext.getCurrentInstance().getExternalContext().getRequestHeaderMap().get("cookie");
        requestHeaders.add("Cookie", headerValue);

        HttpEntity<CustomerDTO> requestEntity = new HttpEntity<>(customerDTO, requestHeaders);

        ResponseEntity<RespondDTO> response = restTemplate.exchange(
                "http://localhost:8080/ws/customer/delete",
                HttpMethod.POST,
                requestEntity,
                RespondDTO.class
        );

        return response.getBody();
    }
}
