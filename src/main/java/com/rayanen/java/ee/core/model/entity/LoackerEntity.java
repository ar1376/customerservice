package com.rayanen.java.ee.core.model.entity;

import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "LOACKER")
public class LoackerEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int ID;

    @Expose
    @Column(name = "CASH")
    private BigDecimal cash;

    @Expose
    @Column(name = "DATE")
    private Date date;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "DEPOSITJOINID")
    private DepositEntity depositEntity;

    public LoackerEntity() {
    }

    public LoackerEntity(BigDecimal cash) {
        this.cash = cash;
        date = new Date();
    }

    public DepositEntity getDepositEntity() {
        return depositEntity;
    }

    public void setDepositEntity(DepositEntity depositEntity) {
        this.depositEntity = depositEntity;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public BigDecimal getCash() {
        return cash;
    }

    public void setCash(BigDecimal cash) {
        this.cash = cash;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}
