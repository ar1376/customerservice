package com.rayanen.java.ee.core.controller.serviceimpl;

import com.rayanen.java.ee.core.controller.IDepositService;
import com.rayanen.java.ee.core.dto.ReportType;
import com.rayanen.java.ee.core.exceptions.*;
import com.rayanen.java.ee.core.log.Log;
import com.rayanen.java.ee.core.model.dao.ICustomerDao;
import com.rayanen.java.ee.core.model.dao.IDepositDao;
import com.rayanen.java.ee.core.model.entity.DepositEntity;
import com.rayanen.java.ee.core.report.Report;
import net.sf.dynamicreports.report.datasource.DRDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

@Component
public class DepositService implements IDepositService {

    private final IDepositDao depositDao;

    private final ICustomerDao customerDao;

    private final Log Log;

    private final Report depositReport;

    @Autowired
    public DepositService(IDepositDao depositDao, ICustomerDao customerDao, Log Log, @Qualifier("DepositReport") Report depositReport) {
        this.depositDao = depositDao;
        this.customerDao = customerDao;
        this.Log = Log;
        this.depositReport = depositReport;
    }

    @Override
    public void insert(String customerID, DepositEntity depositEntity) throws CustomerIDNotFoundException, DepositNumberNotValidException, DepositNumberDuplicateException {
        Log.setInfoLog("Start");
        depositNumberChecker(depositEntity.getDepositNumber());
        depositNumberExistChecker(depositEntity.getDepositNumber());
        depositEntity.setCash(new BigDecimal(0));
        depositEntity.setCustomerEntity(customerDao.searchByCustomerID(customerID));
        depositDao.insert(customerID, depositEntity);
        Log.setInfoLog("End");
    }

    private void depositNumberChecker(String depositNumber) throws DepositNumberNotValidException {
        Log.setInfoLog("Start");
        String emailPattern = "^[0-9]{10}";
        boolean validated = Pattern.matches(emailPattern, depositNumber);

        if (!validated)
            throw new DepositNumberNotValidException();
        Log.setInfoLog("End");
    }

    private void depositNumberExistChecker(String depositNumber) throws DepositNumberDuplicateException {
        Log.setInfoLog("Start");
        if (depositDao.depositNumberChecker(depositNumber))
            throw new DepositNumberDuplicateException();
    }

    @Override
    public void editDepositNumber(String depositNumber, String newDepositNumber) throws DepositNumberNotFoundException, DepositNumberDuplicateException, DepositNumberNotValidException {
        Log.setInfoLog("Start");
        depositNumberChecker(newDepositNumber);
        depositNumberExistChecker(newDepositNumber);
        depositDao.editDepositNumber(depositNumber, newDepositNumber);
        Log.setInfoLog("End");
    }

    @Override
    public void remove(String depositNumber) throws DepositNumberNotFoundException {
        Log.setInfoLog("Start");
        depositDao.remove(depositNumber);
        Log.setInfoLog("End");
    }

    private void cashChecker(BigDecimal cash) throws CashNotValidException {
        Log.setInfoLog("Start");
        if (cash.compareTo(new BigDecimal(0)) < 0)
            throw new CashNotValidException();
        Log.setInfoLog("End");
    }

    @Override
    public synchronized void deposit(BigDecimal cash, String depositNumber) throws DepositNumberNotFoundException, CashNotValidException {
        Log.setInfoLog("Start");
        cashChecker(cash);
        depositDao.deposit(cash, depositNumber);
        Log.setInfoLog("End");
    }

    @Override
    public synchronized void withdraw(BigDecimal cash, String depositNumber) throws DepositNumberNotFoundException, CashNotValidException, NotEnoughCashException {
        Log.setInfoLog("Start");
        cashChecker(cash);
        checkBalance(cash, depositNumber);
        depositDao.withdraw(cash, depositNumber);
        Log.setInfoLog("End");
    }

    private void checkBalance(BigDecimal cash, String depositNumber) throws DepositNumberNotFoundException, NotEnoughCashException {
        Log.setInfoLog("Start");
        BigDecimal cashValue = depositDao.getBalance(depositNumber);
        if (cash.compareTo(cashValue) >= 0)
            throw new NotEnoughCashException();
        Log.setInfoLog("End");
    }

    @Override
    public synchronized void transfer(BigDecimal cash, String depositNumberSender, String depositNumberReceiver) throws CashNotValidException, NotEnoughCashException, SenderWrongDepositNumberException, ReceiverWrongDepositNumberException {
        Log.setInfoLog("Start");
        try {
            withdraw(cash, depositNumberSender);
        } catch (DepositNumberNotFoundException e) {
            Log.setWarnLog(e.getMessage());
            throw new SenderWrongDepositNumberException();
        }
        try {
            deposit(cash, depositNumberReceiver);
        } catch (DepositNumberNotFoundException e) {
            try {
                deposit(cash, depositNumberSender);
            } catch (DepositNumberNotFoundException e1) {
                Log.setWarnLog(e1.getMessage());
            }
            Log.setWarnLog(e.getMessage());
            throw new ReceiverWrongDepositNumberException();
        }
        Log.setInfoLog("End");
    }

    @Override
    public DepositEntity searchByDepositNumber(String depositNumber) throws DepositNumberNotFoundException {
        Log.setInfoLog("Start");
        return depositDao.searchByDepositNumber(depositNumber);
    }

    @Override
    public byte[] getDepositReport(ReportType reportType) {
        Log.setInfoLog("Start");
        DRDataSource dataSource = new DRDataSource("Name", "LastName", "Deposit");
        List<DepositEntity> accountEntities = depositDao.getAll();
        accountEntities.sort(Comparator.comparing(DepositEntity::getCash));
        for (DepositEntity accountEntity : accountEntities) {
            dataSource.add(accountEntity.getCustomerEntity().getName(), accountEntity.getCustomerEntity().getLastName(), accountEntity.getDepositNumber());
        }
        if (reportType.equals(ReportType.PDF))
            return depositReport.getReport(dataSource);
        else
            return depositReport.getExcelReport(dataSource);
    }

//    @Override
//    public void getBenefit() {
//        Log.setInfoLog("Start");
//        List<DepositEntity> depositEntities = depositDao.getAll();
//        BigDecimal min;
//        BigDecimal sum;
//        Boolean flag;
//        for (int i = 0; i < depositEntities.size(); i++) {
//            flag = true;
//            sum = new BigDecimal(0);
//            min = new BigDecimal(0);
//            for (int j = 0; j < depositEntities.get(i).getLoackerEntity().size(); j++) {
//                sum = sum.add(depositEntities.get(i).getLoackerEntity().get(j).getCash());
//                if (depositEntities.get(i).getLoackerEntity().get(j).getDate().compareTo(depositEntities.get(i).getLastBenefit()) >= 0) {
//                    if (flag) {
//                        min = sum;
//                        flag = false;
//                    } else {
//                        if (min.compareTo(sum) > 0)
//                            min = sum;
//                    }
//
//                }
//            }
//            refreshLastBenefitDate(depositEntities.get(i).getDepositNumber());
//            try {
//                deposit(min.divide(new BigDecimal(3650)), depositEntities.get(i).getDepositNumber());
//            } catch (DepositNumberNotFoundException e) {
//                Log.setWarnLog(e.getMessage());
//            } catch (CashNotValidException e) {
//                Log.setWarnLog(e.getMessage());
//            }
//        }
//    }

    @Override
    public void getBenefit() {
        Log.setInfoLog("Start");
        List<DepositEntity> depositEntities = depositDao.getAll();
        BigDecimal min;
        BigDecimal sum;
        BigDecimal allMoney;
        Boolean flag;
        Date oneDay;
        Date todayDate = new Date();
        for (int i = 0; i < depositEntities.size(); i++) {
            oneDay = depositEntities.get(i).getLastBenefit();
            allMoney = new BigDecimal(0);
            while (oneDay.compareTo(todayDate) < 0) {
                flag = true;
                sum = new BigDecimal(0);
                min = new BigDecimal(0);
                for (int j = 0; j < depositEntities.get(i).getLoackerEntity().size(); j++) {
                    sum = sum.add(depositEntities.get(i).getLoackerEntity().get(j).getCash());
                    if (depositEntities.get(i).getLoackerEntity().get(j).getDate().compareTo(oneDay) >= 0
                            && depositEntities.get(i).getLoackerEntity().get(j).getDate().compareTo(new Date(oneDay.getTime() + (1000 * 60 * 60 * 24))) <= 0
                            ) {
                        if (flag) {
                            min = sum;
                            flag = false;
                        } else {
                            if (min.compareTo(sum) > 0)
                                min = sum;
                        }

                    }
                }
                allMoney = allMoney.add(min.divide(new BigDecimal(365),2, RoundingMode.CEILING));

                oneDay = new Date(oneDay.getTime() + (1000 * 60 * 60 * 24));
            }
            try {
                deposit(allMoney.divide(new BigDecimal(10)), depositEntities.get(i).getDepositNumber());
            } catch (DepositNumberNotFoundException e) {
                Log.setWarnLog(e.getMessage());
            } catch (CashNotValidException e) {
                Log.setWarnLog(e.getMessage());
            }
            refreshLastBenefitDate(depositEntities.get(i).getDepositNumber());
        }
    }

    private void refreshLastBenefitDate(String depositNumber) {
        try {
            depositDao.refreshLastBenefitDate(depositNumber);
        } catch (DepositNumberNotFoundException e) {
            e.printStackTrace();
        }
    }
}


