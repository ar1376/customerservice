package com.rayanen.java.ee.core.exceptions;

public class LastNameNotValidException extends Exception {
    @Override
    public String getMessage() {
        return "Last Name Was Not Valid";
    }
}
