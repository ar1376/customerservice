package com.rayanen.java.ee.core.proxy.proxyimp;

import com.rayanen.java.ee.core.dto.ReportType;
import com.rayanen.java.ee.core.dto.RespondDTO;
import com.rayanen.java.ee.core.proxy.IReportProxy;
import com.rayanen.java.ee.core.report.Report;
import com.rayanen.java.ee.core.ws.wrapper.TwoWrapper;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.faces.context.FacesContext;

@Component
public class ReportProxy implements IReportProxy {

    @Override
    public RespondDTO<byte[]> customerReport(ReportType reportType) {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders requestHeaders = new HttpHeaders();

        String headerValue = FacesContext.getCurrentInstance().getExternalContext().getRequestHeaderMap().get("cookie");
        requestHeaders.add("Cookie", headerValue);

        HttpEntity<ReportType> requestEntity = new HttpEntity<>(reportType,requestHeaders);

        ResponseEntity<RespondDTO<byte[]>> response = restTemplate.exchange(
                "http://localhost:8080/ws/report/customer",
                HttpMethod.POST,
                requestEntity,
                new ParameterizedTypeReference<RespondDTO<byte[]>>(){}
        );
        return response.getBody();
    }

    @Override
    public RespondDTO<byte[]> depositsReport(ReportType reportType) {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders requestHeaders = new HttpHeaders();

        String headerValue = FacesContext.getCurrentInstance().getExternalContext().getRequestHeaderMap().get("cookie");
        requestHeaders.add("Cookie", headerValue);

        HttpEntity<ReportType> requestEntity = new HttpEntity<>(reportType,requestHeaders);

        ResponseEntity<RespondDTO<byte[]>> response = restTemplate.exchange(
                "http://localhost:8080/ws/report/deposits",
                HttpMethod.POST,
                requestEntity,
                new ParameterizedTypeReference<RespondDTO<byte[]>>(){}
        );

        return response.getBody();
    }

    @Override
    public RespondDTO<byte[]> depositForACustomer(String name, ReportType reportType) {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders requestHeaders = new HttpHeaders();

        String headerValue = FacesContext.getCurrentInstance().getExternalContext().getRequestHeaderMap().get("cookie");
        requestHeaders.add("Cookie", headerValue);

        TwoWrapper<String,ReportType> twoWrapper = new TwoWrapper<>();
        twoWrapper.setT(name);
        twoWrapper.setE(reportType);

        HttpEntity<TwoWrapper<String,ReportType>> requestEntity = new HttpEntity<>(twoWrapper,requestHeaders);

        ResponseEntity<RespondDTO<byte[]>> response = restTemplate.exchange(
                "http://localhost:8080/ws/report/depositforacustomer",
                HttpMethod.POST,
                requestEntity,
                new ParameterizedTypeReference<RespondDTO<byte[]>>(){}
        );
        return response.getBody();
    }
}
