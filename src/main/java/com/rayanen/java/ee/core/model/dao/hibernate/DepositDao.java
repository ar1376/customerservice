package com.rayanen.java.ee.core.model.dao.hibernate;

import com.rayanen.java.ee.core.exceptions.*;
import com.rayanen.java.ee.core.log.Log;
import com.rayanen.java.ee.core.model.dao.IDepositDao;
import com.rayanen.java.ee.core.model.entity.CustomerEntity;
import com.rayanen.java.ee.core.model.entity.DepositEntity;
import com.rayanen.java.ee.core.model.entity.LoackerEntity;
import com.rayanen.java.ee.core.property.PropertyClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
@Repository
public class DepositDao extends AbstractDao implements IDepositDao {


    private final PropertyClass propertyClass;

    private final Log Log;

    @Autowired
    public DepositDao(PropertyClass propertyClass, Log Log) {
        this.propertyClass = propertyClass;
        this.Log = Log;
    }

    @Override
    public void insert(String customerID, DepositEntity depositEntity) throws CustomerIDNotFoundException {
        Log.setInfoLog("Start");
        CustomerEntity customerEntity;
        try {
            String jpql = propertyClass.getProperty("CustomerID_DB");
            Query q = em.createQuery(jpql);
            q.setParameter("customer_ID", customerID);
            customerEntity = (CustomerEntity) q.getSingleResult();
            customerEntity.getDepositEntities().add(depositEntity);

        } catch (Exception e) {
            Log.setWarnLog(e.getMessage());
            throw new CustomerIDNotFoundException();
        }
        Log.setInfoLog("End");
    }

    @Override
    public boolean depositNumberChecker(String depositNumber) {
        Log.setInfoLog("Start");
        DepositEntity depositEntity;
        try {
            String jpql = propertyClass.getProperty("DepositNumberDB");
            Query q = em.createQuery(jpql);
            q.setParameter("deposit_Number", depositNumber);
            depositEntity = (DepositEntity) q.getSingleResult();

        } catch (Exception e) {
            Log.setWarnLog(e.getMessage());
            return false;
        }
        return depositEntity != null;
    }

    @Override
    public void editDepositNumber(String depositNumber, String newDepositNumber) throws DepositNumberNotFoundException {
        Log.setInfoLog("Start");
        DepositEntity depositEntity;
        try {
            String jpql = propertyClass.getProperty("DepositNumberDB");
            Query q = em.createQuery(jpql);
            q.setParameter("deposit_Number", depositNumber);
            depositEntity = (DepositEntity) q.getSingleResult();
            depositEntity.setDepositNumber(newDepositNumber);
        } catch (Exception e) {
            Log.setWarnLog(e.getMessage());
            throw new DepositNumberNotFoundException();
        }
        Log.setInfoLog("End");
    }

    @Override
    public void remove(String depositNumber) throws DepositNumberNotFoundException {
        Log.setInfoLog("Start");
        DepositEntity depositEntity;
        try {
            String jpql = propertyClass.getProperty("DepositNumberDB");
            Query q = em.createQuery(jpql);
            q.setParameter("deposit_Number", depositNumber);
            depositEntity = (DepositEntity) q.getSingleResult();
            em.remove(depositEntity);
        } catch (Exception e) {
            Log.setWarnLog(e.getMessage());
            throw new DepositNumberNotFoundException();
        }
        Log.setInfoLog("End");
    }

    @Override
    public void deposit(BigDecimal cash, String depositNumber) throws DepositNumberNotFoundException {
        Log.setInfoLog("Start");
        DepositEntity depositEntity;
        try {
            String jpql = propertyClass.getProperty("DepositNumberDB");
            Query q = em.createQuery(jpql);
            q.setParameter("deposit_Number", depositNumber);
            depositEntity = (DepositEntity) q.getSingleResult();
            depositEntity.setCash(depositEntity.getCash().add(cash));
            depositEntity.getLoackerEntity().add(new LoackerEntity(cash));
        } catch (Exception e) {
            Log.setWarnLog(e.getMessage());
            throw new DepositNumberNotFoundException();
        }
        Log.setInfoLog("End");
    }

    @Override
    public void withdraw(BigDecimal cash, String depositNumber) throws DepositNumberNotFoundException {
        Log.setInfoLog("Start");
        DepositEntity depositEntity;
        try {
            String jpql = propertyClass.getProperty("DepositNumberDB");
            Query q = em.createQuery(jpql);
            q.setParameter("deposit_Number", depositNumber);
            depositEntity = (DepositEntity) q.getSingleResult();
            depositEntity.setCash(depositEntity.getCash().add(cash.negate()));
            depositEntity.getLoackerEntity().add(new LoackerEntity(cash.negate()));
        } catch (Exception e) {
            Log.setWarnLog(e.getMessage());
            throw new DepositNumberNotFoundException();
        }
        Log.setInfoLog("End");
    }

    @Override
    public BigDecimal getBalance(String depositNumber) throws DepositNumberNotFoundException {
        Log.setInfoLog("Start");
        DepositEntity depositEntity;
        BigDecimal cash;
        try {
            String jpql = propertyClass.getProperty("DepositNumberDB");
            Query q = em.createQuery(jpql);
            q.setParameter("deposit_Number", depositNumber);
            depositEntity = (DepositEntity) q.getSingleResult();
            cash = depositEntity.getCash();
        } catch (Exception e) {
            Log.setWarnLog(e.getMessage());
            throw new DepositNumberNotFoundException();
        }
        return cash;
    }

    public DepositEntity searchByDepositNumber(String depositNumber) throws DepositNumberNotFoundException {
        Log.setInfoLog("Start");
        DepositEntity depositEntity;
        try {
            String jpql = propertyClass.getProperty("DepositNumberDB");
            Query q = em.createQuery(jpql);
            q.setParameter("deposit_Number", depositNumber);
            depositEntity = (DepositEntity) q.getSingleResult();
        } catch (Exception e) {
            Log.setWarnLog(e.getMessage());
            throw new DepositNumberNotFoundException();
        }
        return depositEntity;
    }

    @Override
    public List<DepositEntity> getAll() {
        Log.setInfoLog("Start");
        List<DepositEntity> depositEntities = null;
        try {
            String jpql = propertyClass.getProperty("AccountEntityDB");
            Query q = em.createQuery(jpql, DepositEntity.class);
            depositEntities = (ArrayList<DepositEntity>) q.getResultList();
        } catch (Exception e) {
            Log.setWarnLog(e.getMessage());
        }
        return depositEntities;
    }

    @Override
    public void removeAll() {
        Log.setInfoLog("Start");
        List<DepositEntity> depositEntities;
        try {
            String jpql = propertyClass.getProperty("AccountEntityDB");
            Query q = em.createQuery(jpql, DepositEntity.class);
            depositEntities = (List<DepositEntity>) q.getResultList();
            for (DepositEntity depositEntity : depositEntities) em.remove(depositEntity);
        } catch (Exception e) {
            Log.setWarnLog(e.getMessage());
        }
        Log.setInfoLog("End");
    }

    @Override
    public void refreshLastBenefitDate(String depositNumber) throws DepositNumberNotFoundException {
        Log.setInfoLog("Start");
        DepositEntity depositEntity;
        try {
            String jpql = propertyClass.getProperty("DepositNumberDB");
            Query q = em.createQuery(jpql);
            q.setParameter("deposit_Number", depositNumber);
            depositEntity = (DepositEntity) q.getSingleResult();
            depositEntity.setLastBenefit(new Date());
        } catch (Exception e) {
            Log.setWarnLog(e.getMessage());
            throw new DepositNumberNotFoundException();
        }
    }
}
