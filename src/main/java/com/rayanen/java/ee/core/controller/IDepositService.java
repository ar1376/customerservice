package com.rayanen.java.ee.core.controller;


import com.rayanen.java.ee.core.dto.ReportType;
import com.rayanen.java.ee.core.exceptions.*;
import com.rayanen.java.ee.core.model.entity.DepositEntity;

import java.math.BigDecimal;

public interface IDepositService {

    void insert(String customerID, DepositEntity depositEntity) throws CustomerIDNotFoundException, DepositNumberNotValidException, DepositNumberDuplicateException;

    void editDepositNumber(String depositNumber, String newDepositNumber) throws DepositNumberNotFoundException, DepositNumberDuplicateException, DepositNumberNotValidException;

    void remove(String depositNumber) throws DepositNumberNotFoundException;

    void deposit(BigDecimal cash, String depositNumber) throws DepositNumberNotFoundException, CashNotValidException;

    void withdraw(BigDecimal cash, String depositNumber) throws DepositNumberNotFoundException, CashNotValidException, NotEnoughCashException;

    void transfer(BigDecimal cash, String depositNumberSender, String depositNumberReceiver) throws CashNotValidException, NotEnoughCashException, SenderWrongDepositNumberException, ReceiverWrongDepositNumberException;

    DepositEntity searchByDepositNumber(String depositNumber) throws DepositNumberNotFoundException;

    byte[] getDepositReport(ReportType reportType);

    void getBenefit();
}
