package com.rayanen.java.ee.core.property;

import com.rayanen.java.ee.core.log.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

@Component("PropertyClass")
public class PropertyClass {

    private final Log Log;
    private static Properties p = new Properties();

    @Autowired
    public PropertyClass(Log Log) {
        this.Log = Log;
    }


//    public void create() {
//        try {
//            p.store(new FileWriter("info.properties"), "comment");
//            p.setProperty("test","test");
//        } catch (IOException e) {
//            e.printStackTrace();
//            Log.setWarnLog(e.getStackTrace().toString());
//        }
//    }

    public String getProperty(String name) {
        load();
        return p.getProperty(name);
    }

    private void load() {
        try {
            ClassLoader classLoader = getClass().getClassLoader();
            FileReader reader = new FileReader(classLoader.getResource("info.properties").getFile());
            p.load(reader);
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
            Log.setWarnLog(e.getMessage());
        }
    }
}
