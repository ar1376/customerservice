package com.rayanen.java.ee.core.ws;

import com.rayanen.java.ee.core.dto.ReportType;
import com.rayanen.java.ee.core.dto.RespondDTO;
import com.rayanen.java.ee.core.exceptions.CustomerNameNotFoundException;
import com.rayanen.java.ee.core.facade.ICustomerFacade;
import com.rayanen.java.ee.core.facade.IDepositFacade;
import com.rayanen.java.ee.core.log.Log;
import com.rayanen.java.ee.core.ws.wrapper.TwoWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/report")
public class ReportRestController {
    private final ICustomerFacade customerFacade;

    private final IDepositFacade depositFacade;

    private final Log Log;

    @Autowired
    public ReportRestController(ICustomerFacade customerFacade, IDepositFacade depositFacade, Log Log) {
        this.customerFacade = customerFacade;
        this.depositFacade = depositFacade;
        this.Log = Log;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/customer")
    public @ResponseBody
    ResponseEntity<RespondDTO<byte[]>> customer(@RequestBody ReportType reportType) {
        RespondDTO<byte[]> respondDTO = new RespondDTO<>();
        respondDTO.setT(customerFacade.getCustomerReport(reportType));
        respondDTO.setMessage("Done");
        return new ResponseEntity<>(respondDTO, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/deposits")
    public @ResponseBody
    ResponseEntity<RespondDTO<byte[]>> deposits(@RequestBody ReportType reportType) {
        RespondDTO<byte[]> respondDTO = new RespondDTO<>();
        respondDTO.setT(depositFacade.getDepositReport(reportType));
        respondDTO.setMessage("Done");
        return new ResponseEntity<>(respondDTO,HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/depositforacustomer")
    public @ResponseBody
    ResponseEntity<RespondDTO<byte[]>> depositForACustomer(@RequestBody TwoWrapper<String,ReportType> twoWrapper) {
        RespondDTO<byte[]> respondDTO = new RespondDTO<>();
        try {
            respondDTO.setT(customerFacade.getDepositForACustomerReport(twoWrapper.getT(),twoWrapper.getE()));
            respondDTO.setMessage("Done");
        } catch (CustomerNameNotFoundException e) {
            Log.setWarnLog(e.getMessage());
            respondDTO.setMessage(e.getMessage());
        }

        return new ResponseEntity<>(respondDTO,HttpStatus.OK);
    }
}

