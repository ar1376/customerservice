package com.rayanen.java.ee.core.ui.reportbean;

import com.rayanen.java.ee.core.dto.ReportType;
import com.rayanen.java.ee.core.dto.RespondDTO;
import com.rayanen.java.ee.core.proxy.IReportProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Scope("view")
public class ReportDepositForACustomerBean {

    private String name;
    private String reportType;

    private final IReportProxy reportProxy;

    @Autowired
    public ReportDepositForACustomerBean(IReportProxy reportProxy) {
        this.reportProxy = reportProxy;
    }

    public String getReportType() {
        return reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void getDepositForACustomerReport(ActionEvent actionEvent) {
        RespondDTO<byte[]> respondDTO = reportProxy.depositForACustomer(name, ReportType.asMyEnum(reportType));
        byte[] bytes = respondDTO.getT();
        if (bytes != null) {
            HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            if (reportType.equals("PDF"))
                httpServletResponse.addHeader("Content-disposition", "attachment; filename=DepositForACustomerReport.pdf");
            else
                httpServletResponse.addHeader("Content-disposition", "attachment; filename=DepositForACustomerReport.xlsx");

            ServletOutputStream servletStream = null;
            try {
                servletStream = httpServletResponse.getOutputStream();
                servletStream.write(bytes);
            } catch (IOException e) {
                e.printStackTrace();
            }
            FacesContext.getCurrentInstance().responseComplete();
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, respondDTO.getMessage(), ""));
    }
}
