package com.rayanen.java.ee.core.facade.conversion;

import com.rayanen.java.ee.core.dto.CustomerDTO;
import com.rayanen.java.ee.core.dto.DepositDTO;
import com.rayanen.java.ee.core.dto.DepositType;
import com.rayanen.java.ee.core.dto.UserPassDTO;
import com.rayanen.java.ee.core.log.Log;
import com.rayanen.java.ee.core.model.entity.CustomerEntity;
import com.rayanen.java.ee.core.model.entity.DepositEntity;
import com.rayanen.java.ee.core.model.entity.UserPassEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

@Component
public class Convert {

    private final Log Log;

    @Autowired
    public Convert(Log Log) {
        this.Log = Log;
    }

    public CustomerEntity DtoToEntity(CustomerDTO customerDTO) {
        Log.setInfoLog("Start");
        String name = customerDTO.getName();
        String lastName = customerDTO.getLastName();
        String email = customerDTO.getEmail();
        String customerID = customerDTO.getCustomerID();
        List<DepositEntity> accounts = new ArrayList<>();
        if (customerDTO.getDepositDTOS() != null)
            IntStream.range(0, customerDTO.getDepositDTOS().size()).forEachOrdered(i -> {
                BigDecimal cash = customerDTO.getDepositDTOS().get(i).getCash();
                String depositNumber = customerDTO.getDepositDTOS().get(i).getDepositNumber();
                DepositType depositType = customerDTO.getDepositDTOS().get(i).getDepositType();
                accounts.add(new DepositEntity(cash, depositNumber, depositType));
            });
        UserPassEntity userPassEntity = new UserPassEntity(customerDTO.getUserPassDTO().getUsername(), customerDTO.getUserPassDTO().getPassword());
        return new CustomerEntity(name, lastName, email, customerID, userPassEntity, accounts);
    }

    public CustomerDTO EntityToDto(CustomerEntity customerEntity) {
        Log.setInfoLog("Start");
        String name = customerEntity.getName();
        String lastName = customerEntity.getLastName();
        String email = customerEntity.getEmail();
        String customerID = customerEntity.getCustomerID();
        List<DepositDTO> accounts = new ArrayList<>();
        if (customerEntity.getDepositEntities() != null)
            IntStream.range(0, customerEntity.getDepositEntities().size()).forEachOrdered(i -> {
                BigDecimal cash = customerEntity.getDepositEntities().get(i).getCash();
                String depositNumber = customerEntity.getDepositEntities().get(i).getDepositNumber();
                DepositType depositType = customerEntity.getDepositEntities().get(i).getDepositType();
                accounts.add(new DepositDTO(cash, depositNumber, depositType));
            });
        UserPassDTO userPassDTO = new UserPassDTO(customerEntity.getUserPassEntity().getUsername(), customerEntity.getUserPassEntity().getPassword());
        return new CustomerDTO(name, lastName, email, customerID, userPassDTO, accounts);
    }

    public DepositEntity AccountDtoToEntity(DepositDTO accountDTO) {
        Log.setInfoLog("Start");
        BigDecimal cash = accountDTO.getCash();
        String depositNumber = accountDTO.getDepositNumber();
        DepositType depositType = accountDTO.getDepositType();
        return new DepositEntity(cash, depositNumber, depositType);
    }

    public DepositDTO AccountEntityToDto(DepositEntity accountEntity) {
        Log.setInfoLog("Start");
        BigDecimal cash = accountEntity.getCash();
        String depositNumber = accountEntity.getDepositNumber();
        DepositType depositType = accountEntity.getDepositType();
        return new DepositDTO(cash, depositNumber, depositType);
    }
}
