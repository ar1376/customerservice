package com.rayanen.java.ee.core.model.dao.hibernate;

import com.rayanen.java.ee.core.exceptions.*;
import com.rayanen.java.ee.core.log.Log;
import com.rayanen.java.ee.core.model.dao.ICustomerDao;
import com.rayanen.java.ee.core.model.entity.CustomerEntity;
import com.rayanen.java.ee.core.model.entity.UserPassEntity;
import com.rayanen.java.ee.core.property.PropertyClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerDao extends AbstractDao implements ICustomerDao {

    private final PropertyClass propertyClass;

    private final Log Log;

    @Autowired
    public CustomerDao(PropertyClass propertyClass, Log Log) {
        this.propertyClass = propertyClass;
        this.Log = Log;
    }

    @Override
    public void insert(CustomerEntity customerEntity) throws StoreFailedException {
        Log.setInfoLog("Start");
        try {
            em.persist(customerEntity);
        } catch (TransactionRequiredException e) {
            Log.setWarnLog(e.getMessage());
            throw new StoreFailedException();
        }
        Log.setInfoLog("End");
    }

    @Override
    public CustomerEntity searchByCustomerID(String customerID) throws CustomerIDNotFoundException {
        Log.setInfoLog("Start");
        CustomerEntity customerEntity;
        try {
            String jpql = propertyClass.getProperty("CustomerID_DB");
            Query q = em.createQuery(jpql);
            q.setParameter("customer_ID", customerID);
            customerEntity = (CustomerEntity) q.getSingleResult();

        } catch (Exception e) {
            Log.setWarnLog(e.getMessage());
            throw new CustomerIDNotFoundException();
        }
        return customerEntity;
    }

    @Override
    public boolean customerIDChecker(String customerID) {
        Log.setInfoLog("Start");
        CustomerEntity customerEntity;
        try {
            String jpql = propertyClass.getProperty("CustomerID_DB");
            Query q = em.createQuery(jpql);
            q.setParameter("customer_ID", customerID);
            customerEntity = (CustomerEntity) q.getSingleResult();

        } catch (Exception e) {
            Log.setWarnLog(e.getMessage());
            return false;
        }
        return customerEntity != null;
    }

    @Override
    public void editName(String customerID, String name) throws CustomerIDNotFoundException {
        Log.setInfoLog("Start");
        CustomerEntity customerEntity;
        try {
            String jpql = propertyClass.getProperty("CustomerID_DB");
            Query q = em.createQuery(jpql);
            q.setParameter("customer_ID", customerID);
            customerEntity = (CustomerEntity) q.getSingleResult();
            customerEntity.setName(name);

        } catch (Exception e) {
            Log.setWarnLog(e.getMessage());
            throw new CustomerIDNotFoundException();
        }
        Log.setInfoLog("End");
    }

    @Override
    public void editLastName(String customerID, String lastName) throws CustomerIDNotFoundException {
        Log.setInfoLog("Start");
        CustomerEntity customerEntity;
        try {
            String jpql = propertyClass.getProperty("CustomerID_DB");
            Query q = em.createQuery(jpql);
            q.setParameter("customer_ID", customerID);
            customerEntity = (CustomerEntity) q.getSingleResult();
            customerEntity.setLastName(lastName);

        } catch (Exception e) {
            Log.setWarnLog(e.getMessage());
            throw new CustomerIDNotFoundException();
        }
        Log.setInfoLog("End");
    }

    @Override
    public void remove(String customerID) throws CustomerIDNotFoundException {
        Log.setInfoLog("Start");
        CustomerEntity customerEntity;
        try {
            String jpql = propertyClass.getProperty("CustomerID_DB");
            Query q = em.createQuery(jpql);
            q.setParameter("customer_ID", customerID);
            customerEntity = (CustomerEntity) q.getSingleResult();
            em.remove(customerEntity);
        } catch (Exception e) {
            Log.setWarnLog(e.getMessage());
            throw new CustomerIDNotFoundException();
        }
        Log.setInfoLog("End");
    }

    @Override
    public List<CustomerEntity> getAll() {
        Log.setInfoLog("Start");
        List<CustomerEntity> customerEntity = null;
        try {
            String jpql = propertyClass.getProperty("CustomerEntityDB");
            Query q = em.createQuery(jpql, CustomerEntity.class);
            customerEntity = (ArrayList<CustomerEntity>) q.getResultList();
        } catch (Exception e) {
            Log.setWarnLog(e.getMessage());
        }
        return customerEntity;
    }

    @Override
    public CustomerEntity searchByCustomerName(String name) throws CustomerNameNotFoundException {
        Log.setInfoLog("Start");
        CustomerEntity customerEntity;
        try {
            String jpql = propertyClass.getProperty("NameDB");
            Query q = em.createQuery(jpql);
            q.setParameter("name_", name);
            customerEntity = (CustomerEntity) q.getSingleResult();

        } catch (Exception e) {
            Log.setWarnLog(e.getMessage());
            throw new CustomerNameNotFoundException();
        }
        return customerEntity;
    }

    @Override
    public List<CustomerEntity> exportBackUp() {
        Log.setInfoLog("Start");
        List<CustomerEntity> customerEntity = null;
        try {
            String jpql = propertyClass.getProperty("CustomerEntityDB");
            Query q = em.createQuery(jpql, CustomerEntity.class);
            customerEntity = (ArrayList<CustomerEntity>) q.getResultList();
        } catch (Exception e) {
            Log.setWarnLog(e.getMessage());
        }
        return customerEntity;
    }

    @Override
    public void removeAll() {
        Log.setInfoLog("Start");
//        Query query = em.createQuery("DELETE FROM CUTOMER");
//        query.executeUpdate();
//        Query query1 = em.createQuery("DELETE FROM DEPOSIT");
//        query1.executeUpdate();
//        Query query2 = em.createQuery("DELETE FROM LOACKER");
//        query2.executeUpdate();
//        Query query3 = em.createQuery("DELETE FROM USER_ROLE");
//        query3.executeUpdate();
//        Query query4 = em.createQuery("DELETE FROM USERPASS");
//        query4.executeUpdate();
//        Query query5 = em.createQuery("DELETE FROM USERPASS_USER_ROLE");
//        query5.executeUpdate();
        List<CustomerEntity> customerEntity;
        try {
            String jpql = propertyClass.getProperty("CustomerEntityDB");
            Query q = em.createQuery(jpql, CustomerEntity.class);
            customerEntity = (ArrayList<CustomerEntity>) q.getResultList();
            for (CustomerEntity aCustomerEntity : customerEntity) em.remove(aCustomerEntity);
        } catch (Exception e) {
            Log.setWarnLog(e.getMessage());
        }
        Log.setInfoLog("End");
    }

    @Override
    public void importBackUp(List<CustomerEntity> customerEntityBackup) {
        Log.setInfoLog("Start");
        try {

            for (CustomerEntity aCustomerEntityBackup : customerEntityBackup) {
                em.persist(aCustomerEntityBackup);
            }

        } catch (Exception e) {
            Log.setWarnLog(e.getMessage());
        }
        Log.setInfoLog("End");
    }

    @Override
    public List<UserPassEntity> getAllUserPass() {
        Log.setInfoLog("Start");
        List<UserPassEntity> userPassEntities = null;
        try {
            String jpql = propertyClass.getProperty("UserPassDB");
            Query q = em.createQuery(jpql, UserPassEntity.class);
            userPassEntities = (ArrayList<UserPassEntity>) q.getResultList();
        } catch (Exception e) {
            Log.setWarnLog(e.getMessage());
        }
        return userPassEntities;
    }

    public UserPassEntity findByUserName(String username) {

        //JPQL Syntax:
        UserPassEntity userPassEntity = null;
        String jpql = "select u from UserPassEntity u where u.username = :username";
        Query q = em.createQuery(jpql);
        q.setParameter("username", username);
        try {
            userPassEntity = (UserPassEntity) q.getSingleResult();
        } catch (Exception e) {
            System.out.println("Not Found!");
        }
        return userPassEntity;

    }

    public void insert(UserPassEntity userPassEntity) {
        CustomerEntity customerEntity = new CustomerEntity();
        customerEntity.setUserPassEntity(userPassEntity);
        em.persist(customerEntity);
    }

}
