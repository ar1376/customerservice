package com.rayanen.java.ee.core.model.dao;


import com.rayanen.java.ee.core.exceptions.*;
import com.rayanen.java.ee.core.model.entity.DepositEntity;

import java.math.BigDecimal;
import java.util.List;

public interface IDepositDao {

    void insert(String customerID, DepositEntity depositEntity) throws CustomerIDNotFoundException;

    boolean depositNumberChecker(String depositNumber);

    void editDepositNumber(String depositNumber, String newDepositNumber) throws DepositNumberNotFoundException;

    void remove(String depositNumber) throws DepositNumberNotFoundException;

    void deposit(BigDecimal cash, String depositNumber) throws DepositNumberNotFoundException;

    void withdraw(BigDecimal cash, String depositNumber) throws DepositNumberNotFoundException;

    BigDecimal getBalance(String depositNumber) throws DepositNumberNotFoundException;

    DepositEntity searchByDepositNumber(String depositNumber) throws DepositNumberNotFoundException;

    List<DepositEntity> getAll();

    void removeAll();

    void refreshLastBenefitDate(String depositNumber) throws DepositNumberNotFoundException;
}
