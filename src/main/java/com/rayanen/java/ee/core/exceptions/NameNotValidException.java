package com.rayanen.java.ee.core.exceptions;

public class NameNotValidException extends Exception {
    @Override
    public String getMessage() {
        return "Name Was Not Valid";
    }
}
