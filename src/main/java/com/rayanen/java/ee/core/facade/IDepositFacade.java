package com.rayanen.java.ee.core.facade;


import com.rayanen.java.ee.core.dto.DepositDTO;
import com.rayanen.java.ee.core.dto.ReportType;
import com.rayanen.java.ee.core.exceptions.*;
import org.springframework.security.access.prepost.PreAuthorize;

import java.math.BigDecimal;

public interface IDepositFacade {

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    void insert(String customerID, DepositDTO depositDTO) throws DepositNumberNotValidException, CustomerIDNotFoundException, DepositNumberDuplicateException;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    void editDepositNumber(String depositNumber, String newDepositNumber) throws DepositNumberNotFoundException, DepositNumberNotValidException, DepositNumberDuplicateException;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    void remove(String depositNumber) throws DepositNumberNotFoundException;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    void deposit(BigDecimal cash, String depositNumber) throws CashNotValidException, DepositNumberNotFoundException;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    void withdraw(BigDecimal cash, String depositNumber) throws CashNotValidException, NotEnoughCashException, DepositNumberNotFoundException;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    void transfer(BigDecimal cash, String depositNumberSender, String depositNumberReceiver) throws SenderWrongDepositNumberException, NotEnoughCashException, ReceiverWrongDepositNumberException, CashNotValidException;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    DepositDTO searchByDepositNumber(String depositNumber) throws DepositNumberNotFoundException;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    byte[] getDepositReport(ReportType reportType);

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    void getBenefit();
}
