package com.rayanen.java.ee.core.ui.depositbean;

import com.rayanen.java.ee.core.dto.RespondDTO;
import com.rayanen.java.ee.core.proxy.IDepositProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

@Component
@Scope("view")
public class DepositDeleteBean {
    private String depositNumber;

    final
    IDepositProxy depositProxy;

    @Autowired
    public DepositDeleteBean(IDepositProxy depositProxy) {
        this.depositProxy = depositProxy;
    }

    public String getDepositNumber() {
        return depositNumber;
    }

    public void setDepositNumber(String depositNumber) {
        this.depositNumber = depositNumber;
    }

    public void delete(ActionEvent actionEvent) {
        RespondDTO response = depositProxy.deleteDeposit(depositNumber);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, response.getMessage(), ""));
    }
}
