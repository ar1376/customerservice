package com.rayanen.java.ee.core.exceptions;

public class CustomerIDDuplicateException extends Exception {
    @Override
    public String getMessage() {
        return "Customer ID Already Exist !";
    }
}
