package com.rayanen.java.ee.core.report;

import com.rayanen.java.ee.core.log.Log;
import com.rayanen.java.ee.core.report.theme.ReportTheme;
import net.sf.dynamicreports.jasper.builder.export.JasperPdfExporterBuilder;
import net.sf.dynamicreports.jasper.builder.export.JasperXlsxExporterBuilder;
import net.sf.dynamicreports.report.builder.component.HorizontalListBuilder;
import net.sf.dynamicreports.report.builder.component.ImageBuilder;
import net.sf.dynamicreports.report.builder.component.RectangleBuilder;
import net.sf.dynamicreports.report.builder.style.StyleBuilder;
import net.sf.dynamicreports.report.constant.ImageScale;
import net.sf.dynamicreports.report.exception.DRException;
import net.sf.jasperreports.engine.JRDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static net.sf.dynamicreports.report.builder.DynamicReports.*;

@Component("DepositReport")
public class DepositReport extends Report {

    private final Log Log;

    @Autowired
    public DepositReport(Log Log) {
        this.Log = Log;
    }

    public byte[] getReport(JRDataSource jrDataSource) {
        Log.setInfoLog("Start");
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        JasperPdfExporterBuilder pdfExporter = export.pdfExporter(buffer);
        StyleBuilder style1 = stl.style()
                .setRadius(10)
                .setBackgroundColor(new Color(230, 230, 230))
                .setLinePen(stl.pen().setLineColor(Color.LIGHT_GRAY));
        StyleBuilder style2 = stl.style()
                .setRadius(5);
        ImageBuilder background1 = cmp.image(ReportTheme.class.getResource("images/background.gif"))
                .setImageScale(ImageScale.CLIP)
                .setStyle(style1);
        RectangleBuilder background2 = cmp.rectangle()
                .setStyle(style2);
        RectangleBuilder background3 = cmp.rectangle()
                .setStyle(style1)
                .setPrintWhenExpression(exp.printInOddRow());

        HorizontalListBuilder title1 = cmp.horizontalList()
                .add(cmp.text("Ahmadreza"))
                .setBackgroundComponent(background2);
        HorizontalListBuilder title2 = cmp.horizontalList()
                .add(cmp.text("Reyhani"))
                .setBackgroundComponent(background2);
        HorizontalListBuilder title = cmp.horizontalList()
                .add(title1, cmp.horizontalGap(20), title2)
                .setStyle(stl.style(10));
        try {
            report()
                    .setColumnStyle(ReportTheme.columnStyle)
                    .setColumnTitleStyle(ReportTheme.boldCenteredStyle)
                    .setTitleBackgroundComponent(background1)
                    .setColumnHeaderBackgroundComponent(background2)
                    .setPageFooterBackgroundComponent(background1)
                    .setDetailBackgroundComponent(background3)
                    .columns(
                            col.column("Name", "Name", type.stringType()),
                            col.column("LastName", "LastName", type.stringType()),
                            col.column("Deposit", "Deposit", type.stringType()))

                    .title(
                            ReportTheme.createTitleComponent("Deposit List"),
                            title)
                    .pageFooter(cmp.pageXofY().setStyle(ReportTheme.boldCenteredStyle))
                    .setDataSource(jrDataSource)
                    .toPdf(pdfExporter);
            buffer.flush();
            buffer.close();
        } catch (DRException e) {
            e.printStackTrace();
            Log.setWarnLog(e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                buffer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return buffer.toByteArray();
    }

    public byte[] getExcelReport(JRDataSource jrDataSource) {
        Log.setInfoLog("Start");
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        JasperXlsxExporterBuilder xlsxExporter = export.xlsxExporter(buffer)
                .setDetectCellType(true)
                .setIgnorePageMargins(true)
                .setWhitePageBackground(false)
                .setRemoveEmptySpaceBetweenColumns(true);
        try {
            report()
                    .ignorePageWidth()
                    .ignorePagination()
                    .columns(
                            col.column("Name", "Name", type.stringType()),
                            col.column("LastName", "LastName", type.stringType()),
                            col.column("Deposit", "Deposit", type.stringType()))
                    .setDataSource(jrDataSource)
                    .toXlsx(xlsxExporter);
            buffer.flush();
            buffer.close();
        } catch (DRException e) {
            e.printStackTrace();
            Log.setWarnLog(e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                buffer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return buffer.toByteArray();
    }
}
