package com.rayanen.java.ee.core.dto;

import java.math.BigDecimal;
import java.util.Date;

public class LoackerDTO {

    private BigDecimal cash;
    private Date date;

    public LoackerDTO() {
    }

    public LoackerDTO(BigDecimal cash) {
        this.cash = cash;
        this.date = new Date();
    }

    public BigDecimal getCash() {
        return cash;
    }

    public void setCash(BigDecimal cash) {
        this.cash = cash;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
