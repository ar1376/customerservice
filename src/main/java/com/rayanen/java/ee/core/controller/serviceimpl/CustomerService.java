package com.rayanen.java.ee.core.controller.serviceimpl;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.rayanen.java.ee.core.controller.ICustomerService;
import com.rayanen.java.ee.core.controller.IDepositService;
import com.rayanen.java.ee.core.dto.ReportType;
import com.rayanen.java.ee.core.exceptions.*;
import com.rayanen.java.ee.core.log.Log;
import com.rayanen.java.ee.core.model.dao.ICustomerDao;
import com.rayanen.java.ee.core.model.entity.CustomerEntity;
import com.rayanen.java.ee.core.model.entity.DepositEntity;
import com.rayanen.java.ee.core.model.entity.UserPassEntity;
import com.rayanen.java.ee.core.model.entity.UserRole;
import com.rayanen.java.ee.core.report.Report;
import net.sf.dynamicreports.report.datasource.DRDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.lang.reflect.Type;
import java.util.*;
import java.util.regex.Pattern;

@Component
public class CustomerService implements ICustomerService {

    private final ICustomerDao customerDao;

    private final IDepositService depositServise;

    private final PasswordEncoder passwordEncoder;

    private final Log Log;

    private final Report customerReport;

    private final Report depositForACustomerReport;

    @Autowired
    public CustomerService(ICustomerDao customerDao, IDepositService depositServise, PasswordEncoder passwordEncoder, Log Log, @Qualifier("CustomerReport") Report customerReport, @Qualifier("DepositForACustomerReport") Report depositForACustomerReport) {
        this.customerDao = customerDao;
        this.depositServise = depositServise;
        this.passwordEncoder = passwordEncoder;
        this.Log = Log;
        this.customerReport = customerReport;
        this.depositForACustomerReport = depositForACustomerReport;
    }

    @Override
    public void insert(CustomerEntity customerEntity) throws StoreFailedException, CustomerIDDuplicateException, EmailNotValidException, CustomerIDNotValidExeption, LastNameNotValidException, NameNotValidException {
        Log.setInfoLog("Start");
        nameValidChecker(customerEntity);
        customerIDChecker(customerEntity);
        customerIDExistChecker(customerEntity);
        emailValidChecker(customerEntity);
        customerEntity.getUserPassEntity().setPassword(passwordEncoder.encode(customerEntity.getUserPassEntity().getPassword()));
        customerEntity.getUserPassEntity().setEnabled(true);
        customerEntity.getUserPassEntity().getUserRole().add(new UserRole("ROLE_USER"));
        customerDao.insert(customerEntity);
        Log.setInfoLog("End");
    }

    private void customerIDExistChecker(CustomerEntity customerEntity) throws CustomerIDDuplicateException {
        Log.setInfoLog("Start");
        if (customerDao.customerIDChecker(customerEntity.getCustomerID()))
            throw new CustomerIDDuplicateException();
        Log.setInfoLog("End");
    }

    private void emailValidChecker(CustomerEntity customerEntity) throws EmailNotValidException {
        Log.setInfoLog("Start");
        String emailPattern = "^[(\\w)]+@[(\\w)]+\\.[(\\w)]+$";
        boolean validated = Pattern.matches(emailPattern, customerEntity.getEmail());

        if (!validated)
            throw new EmailNotValidException();
        Log.setInfoLog("End");
    }

    private void nameValidChecker(CustomerEntity customerEntity) throws LastNameNotValidException, NameNotValidException {
        Log.setInfoLog("Start");
        String namePattern = "^[a-zA-Z\\s]+";
        boolean validated = Pattern.matches(namePattern, customerEntity.getName());
        if (!validated)
            throw new NameNotValidException();
        validated = Pattern.matches(namePattern, customerEntity.getLastName());
        if (!validated)
            throw new LastNameNotValidException();
        Log.setInfoLog("End");
    }

    private void customerIDChecker(CustomerEntity customerEntity) throws CustomerIDNotValidExeption {
        Log.setInfoLog("Start");
        String emailPattern = "^[0-9]{8}";
        boolean validated = Pattern.matches(emailPattern, customerEntity.getCustomerID());

        if (!validated)
            throw new CustomerIDNotValidExeption();
        Log.setInfoLog("End");
    }

    @Override
    public CustomerEntity searchByCustomerID(String customerID) throws CustomerIDNotFoundException {
        Log.setInfoLog("Start");
        return customerDao.searchByCustomerID(customerID);
    }

    @Override
    public void editName(CustomerEntity customerEntity, String name) throws CustomerIDNotFoundException {
        Log.setInfoLog("Start");
        customerDao.editName(customerEntity.getCustomerID(), name);
        Log.setInfoLog("End");
    }

    @Override
    public void editLastName(CustomerEntity customerEntity, String lastName) throws CustomerIDNotFoundException {
        Log.setInfoLog("Start");
        customerDao.editLastName(customerEntity.getCustomerID(), lastName);
        Log.setInfoLog("End");
    }

    @Override
    public void remove(String customerID) throws CustomerIDNotFoundException {
        Log.setInfoLog("Start");
        customerDao.remove(customerID);
        Log.setInfoLog("End");
    }

    @Override
    public CustomerEntity getCustomerByDepositNumber(String depositNumber) throws DepositNumberNotFoundException {
        Log.setInfoLog("Start");
        return depositServise.searchByDepositNumber(depositNumber).getCustomerEntity();
    }

    @Override
    public byte[] getCustomerReport(ReportType reportType) {
        Log.setInfoLog("Start");
        DRDataSource dataSource = new DRDataSource("Name", "LastName", "Email");
        List<CustomerEntity> customerEntityList = customerDao.getAll();
        for (CustomerEntity aCustomerEntityList : customerEntityList) {
            dataSource.add(aCustomerEntityList.getName(), aCustomerEntityList.getLastName(), aCustomerEntityList.getEmail());
        }
        if (reportType.equals(ReportType.PDF))
            return customerReport.getReport(dataSource);
        else
            return customerReport.getExcelReport(dataSource);
    }

    @Override
    public byte[] getDepositForACustomerReport(String name, ReportType reportType) throws CustomerNameNotFoundException {
        Log.setInfoLog("Start");
        DRDataSource dataSource = new DRDataSource("Name", "LastName", "Email", "Deposit");
        CustomerEntity customerEntity = customerDao.searchByCustomerName(name);
        customerEntity.getDepositEntities().sort(Comparator.comparing(DepositEntity::getDepositNumber));
        for (int i = 0; i < customerEntity.getDepositEntities().size(); i++)
            dataSource.add(customerEntity.getName(), customerEntity.getLastName(), customerEntity.getEmail(), customerEntity.getDepositEntities().get(i).getDepositNumber());
        if (reportType.equals(ReportType.PDF))
            return depositForACustomerReport.getReport(dataSource);
        else
            return depositForACustomerReport.getExcelReport(dataSource);
    }

    @Override
    public byte[] exportBackUp() {
        Log.setInfoLog("Start");
        List<CustomerEntity> customerEntities = customerDao.exportBackUp();
        Type type = new TypeToken<List<CustomerEntity>>() {
        }.getType();
        // Convert the object to a JSON string
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        String json = gson.toJson(customerEntities, type);
        return json.getBytes();
    }

    @Override
    public void removeAll() {
        customerDao.removeAll();
    }

    @Override
    public void importBackUp(byte[] bytes) {
        Log.setInfoLog("Start");
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        Type type = new TypeToken<List<CustomerEntity>>() {
        }.getType();

        List<CustomerEntity> customerEntities = gson.fromJson(new String(bytes), type);

        customerDao.importBackUp(customerEntities);

        Log.setInfoLog("End");
    }

    @Override
    public boolean checkUserPass(UserPassEntity userPassEntity) {
        List<UserPassEntity> userPassEntities = customerDao.getAllUserPass();
        for (UserPassEntity userPassEntity1 : userPassEntities) {
            if (userPassEntity1.getUsername().equals(userPassEntity.getUsername()) && userPassEntity1.getPassword().equals(userPassEntity.getPassword()))
                return true;
        }
        return false;
    }

    @Override
    public void insertUser(String username, String password, String roleName) {
        UserPassEntity user = customerDao.findByUserName(username);
        if (user == null) {
            user = new UserPassEntity();
            user.setPassword(passwordEncoder.encode(password));
            user.setUsername(username);
            user.setEnabled(true);
            HashSet<UserRole> userRole = new HashSet<>();
            UserRole role = new UserRole();
            role.setRole(roleName);
            role.setUserPassEntity(user);
            userRole.add(role);
            user.setUserRole(userRole);
            customerDao.insert(user);
        }
    }

}
