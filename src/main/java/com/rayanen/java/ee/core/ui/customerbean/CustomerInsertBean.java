package com.rayanen.java.ee.core.ui.customerbean;


import com.rayanen.java.ee.core.dto.CustomerDTO;
import com.rayanen.java.ee.core.dto.RespondDTO;
import com.rayanen.java.ee.core.proxy.ICustomerProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import java.io.Serializable;

@Component
@Scope("view")
public class CustomerInsertBean implements Serializable {


    private CustomerDTO customerDTO = new CustomerDTO();
    private String lable;

    private final ICustomerProxy iCustomerProxy;

    @Autowired
    public CustomerInsertBean(ICustomerProxy iCustomerProxy) {
        this.iCustomerProxy = iCustomerProxy;
    }

    public String getLable() {
        return lable;
    }

    public void setLable(String lable) {
        this.lable = lable;
    }

    public CustomerDTO getCustomerDTO() {
        return customerDTO;
    }

    public void setCustomerDTO(CustomerDTO customerDTO) {
        this.customerDTO = customerDTO;
    }

    public void insert(ActionEvent actionEvent) {
        lable = customerDTO.getName() + " -  " + customerDTO.getLastName();
        RespondDTO response = iCustomerProxy.insertCustomer(customerDTO);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                response.getMessage(), ""));
    }

}