package com.rayanen.java.ee.core.exceptions;

public class DepositNumberNotFoundException extends  Exception {
    @Override
    public String getMessage() {
        return "Deposit Number Does Not Exist !";
    }
}
