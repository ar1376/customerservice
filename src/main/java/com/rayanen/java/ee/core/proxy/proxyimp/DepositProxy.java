package com.rayanen.java.ee.core.proxy.proxyimp;

import com.rayanen.java.ee.core.dto.DepositDTO;
import com.rayanen.java.ee.core.dto.RespondDTO;
import com.rayanen.java.ee.core.proxy.IDepositProxy;
import com.rayanen.java.ee.core.ws.wrapper.ThreeWrapper;
import com.rayanen.java.ee.core.ws.wrapper.TwoWrapper;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.faces.context.FacesContext;
import java.math.BigDecimal;

@Component
public class DepositProxy implements IDepositProxy {

    @Override
    public RespondDTO insertDeposit(String customerID, DepositDTO depositDTO) {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders requestHeaders = new HttpHeaders();

        TwoWrapper<DepositDTO,String> wrapper = new TwoWrapper<>(depositDTO,customerID);

        String headerValue = FacesContext.getCurrentInstance().getExternalContext().getRequestHeaderMap().get("cookie");
        requestHeaders.add("Cookie", headerValue);

        HttpEntity<TwoWrapper> requestEntity = new HttpEntity<>(wrapper, requestHeaders);

        ResponseEntity<RespondDTO> response = restTemplate.exchange(
                "http://localhost:8080/ws/deposit/insert",
                HttpMethod.POST,
                requestEntity,
                RespondDTO.class
        );

        return response.getBody();
    }

    @Override
    public RespondDTO updateDeposit(String depositNumber , String newDepositNumber) {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders requestHeaders = new HttpHeaders();

        TwoWrapper<String,String> wrapper = new TwoWrapper<>(depositNumber,newDepositNumber);

        String headerValue = FacesContext.getCurrentInstance().getExternalContext().getRequestHeaderMap().get("cookie");
        requestHeaders.add("Cookie", headerValue);

        HttpEntity<TwoWrapper> requestEntity = new HttpEntity<>(wrapper, requestHeaders);

        ResponseEntity<RespondDTO> response = restTemplate.exchange(
                "http://localhost:8080/ws/deposit/update",
                HttpMethod.POST,
                requestEntity,
                RespondDTO.class
        );
        return response.getBody();
    }

    @Override
    public RespondDTO deleteDeposit(String depositNumber) {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders requestHeaders = new HttpHeaders();

        String headerValue = FacesContext.getCurrentInstance().getExternalContext().getRequestHeaderMap().get("cookie");
        requestHeaders.add("Cookie", headerValue);

        HttpEntity<String> requestEntity = new HttpEntity<>(depositNumber, requestHeaders);

        ResponseEntity<RespondDTO> response = restTemplate.exchange(
                "http://localhost:8080/ws/deposit/delete",
                HttpMethod.POST,
                requestEntity,
                RespondDTO.class
        );
        return response.getBody();
    }

    @Override
    public RespondDTO depositDeposit(BigDecimal cash , String depositNumber) {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders requestHeaders = new HttpHeaders();

        String headerValue = FacesContext.getCurrentInstance().getExternalContext().getRequestHeaderMap().get("cookie");
        requestHeaders.add("Cookie", headerValue);

        TwoWrapper<BigDecimal,String> wrapper = new TwoWrapper<>(cash,depositNumber);

        HttpEntity<TwoWrapper> requestEntity = new HttpEntity<>(wrapper, requestHeaders);

        ResponseEntity<RespondDTO> response = restTemplate.exchange(
                "http://localhost:8080/ws/deposit/deposit",
                HttpMethod.POST,
                requestEntity,
                RespondDTO.class
        );

        return response.getBody();
    }

    @Override
    public RespondDTO withdrawDeposit(BigDecimal cash , String depositNumber) {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders requestHeaders = new HttpHeaders();

        String headerValue = FacesContext.getCurrentInstance().getExternalContext().getRequestHeaderMap().get("cookie");
        requestHeaders.add("Cookie", headerValue);

        TwoWrapper<BigDecimal,String> wrapper = new TwoWrapper<>(cash,depositNumber);

        HttpEntity<TwoWrapper> requestEntity = new HttpEntity<>(wrapper, requestHeaders);

        ResponseEntity<RespondDTO> response = restTemplate.exchange(
                "http://localhost:8080/ws/deposit/withdraw",
                HttpMethod.POST,
                requestEntity,
                RespondDTO.class
        );

        return response.getBody();
    }

    @Override
    public RespondDTO transferDeposit(String depositNumberSender,String depositNumberReceiver,BigDecimal cash) {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders requestHeaders = new HttpHeaders();

        String headerValue = FacesContext.getCurrentInstance().getExternalContext().getRequestHeaderMap().get("cookie");
        requestHeaders.add("Cookie", headerValue);

        ThreeWrapper<String,String,BigDecimal> wrapper = new ThreeWrapper<>(depositNumberSender,depositNumberReceiver,cash);

        HttpEntity<ThreeWrapper> requestEntity = new HttpEntity<>(wrapper, requestHeaders);

        ResponseEntity<RespondDTO> response = restTemplate.exchange(
                "http://localhost:8080/ws/deposit/transfer",
                HttpMethod.POST,
                requestEntity,
                RespondDTO.class
        );

        return response.getBody();
    }
}
