package com.rayanen.java.ee.core.ws;

import com.rayanen.java.ee.core.dto.DepositDTO;
import com.rayanen.java.ee.core.dto.RespondDTO;
import com.rayanen.java.ee.core.exceptions.*;
import com.rayanen.java.ee.core.facade.IDepositFacade;
import com.rayanen.java.ee.core.log.Log;
import com.rayanen.java.ee.core.ws.wrapper.ThreeWrapper;
import com.rayanen.java.ee.core.ws.wrapper.TwoWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

@RestController
@RequestMapping(value = "/deposit")
public class DepositRestController {

    private final IDepositFacade depositFacade;

    private final Log Log;

    @Autowired
    public DepositRestController(IDepositFacade depositFacade, Log Log) {
        this.depositFacade = depositFacade;
        this.Log = Log;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/insert")
    public @ResponseBody
    ResponseEntity<RespondDTO> insert(@RequestBody TwoWrapper<DepositDTO, String> wrapper) {
        RespondDTO respondDTO = new RespondDTO();
        try {
            depositFacade.insert(wrapper.getE(), wrapper.getT());
            respondDTO.setMessage("Added Successfully");
        } catch (DepositNumberNotValidException e) {
            Log.setWarnLog(e.getMessage());
            respondDTO.setMessage(e.getMessage());
        } catch (CustomerIDNotFoundException e) {
            Log.setWarnLog(e.getMessage());
            respondDTO.setMessage(e.getMessage());
        } catch (DepositNumberDuplicateException e) {
            Log.setWarnLog(e.getMessage());
            respondDTO.setMessage(e.getMessage());
        }
        return new ResponseEntity<>(respondDTO, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/update")
    public @ResponseBody
    ResponseEntity<RespondDTO> update(@RequestBody TwoWrapper<String, String> wrapper) {
        RespondDTO respondDTO = new RespondDTO();
        try {
            depositFacade.editDepositNumber(wrapper.getT(), wrapper.getE());
            respondDTO.setMessage("Edited Successfully");
        } catch (DepositNumberNotFoundException e) {
            Log.setWarnLog(e.getMessage());
            respondDTO.setMessage(e.getMessage());
        } catch (DepositNumberNotValidException e) {
            Log.setWarnLog(e.getMessage());
            respondDTO.setMessage(e.getMessage());
        } catch (DepositNumberDuplicateException e) {
            Log.setWarnLog(e.getMessage());
            respondDTO.setMessage(e.getMessage());
        }
        return new ResponseEntity<>(respondDTO, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/delete")
    public @ResponseBody
    ResponseEntity<RespondDTO> delete(@RequestBody String depositNumber) {
        RespondDTO respondDTO = new RespondDTO();
        try {
            depositFacade.remove(depositNumber);
            respondDTO.setMessage("Removed Successfully");
        } catch (DepositNumberNotFoundException e) {
            Log.setWarnLog(e.getMessage());
            respondDTO.setMessage(e.getMessage());
        }
        return new ResponseEntity<>(respondDTO, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/deposit")
    public @ResponseBody
    ResponseEntity<RespondDTO> deposit(@RequestBody TwoWrapper<BigDecimal, String> wrapper) {
        RespondDTO respondDTO = new RespondDTO();
        try {
            depositFacade.deposit(wrapper.getT(), wrapper.getE());
            respondDTO.setMessage("Deposit Successfully");
        } catch (CashNotValidException e) {
            Log.setWarnLog(e.getMessage());
            respondDTO.setMessage(e.getMessage());
        } catch (DepositNumberNotFoundException e) {
            Log.setWarnLog(e.getMessage());
            respondDTO.setMessage(e.getMessage());
        }
        return new ResponseEntity<>(respondDTO, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/withdraw")
    public @ResponseBody
    ResponseEntity<RespondDTO> withdraw(@RequestBody TwoWrapper<BigDecimal, String> wrapper) {
        RespondDTO respondDTO = new RespondDTO();
        try {
            depositFacade.withdraw(wrapper.getT(), wrapper.getE());
            respondDTO.setMessage("Withdraw Successfully !");
        } catch (CashNotValidException e) {
            Log.setWarnLog(e.getMessage());
            respondDTO.setMessage(e.getMessage());
        } catch (NotEnoughCashException e) {
            Log.setWarnLog(e.getMessage());
            respondDTO.setMessage(e.getMessage());
        } catch (DepositNumberNotFoundException e) {
            Log.setWarnLog(e.getMessage());
            respondDTO.setMessage(e.getMessage());
        }

        return new ResponseEntity<>(respondDTO, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/transfer")
    public @ResponseBody
    ResponseEntity<RespondDTO> transfer(@RequestBody ThreeWrapper<String, String, BigDecimal> wrapper) {
        RespondDTO respondDTO = new RespondDTO();
        try {
            depositFacade.transfer(wrapper.getF(), wrapper.getT(), wrapper.getE());
            respondDTO.setMessage("Transfer Successfully !");
        } catch (SenderWrongDepositNumberException e) {
            Log.setWarnLog(e.getMessage());
            respondDTO.setMessage(e.getMessage());
        } catch (NotEnoughCashException e) {
            Log.setWarnLog(e.getMessage());
            respondDTO.setMessage(e.getMessage());
        } catch (ReceiverWrongDepositNumberException e) {
            Log.setWarnLog(e.getMessage());
            respondDTO.setMessage(e.getMessage());
        } catch (CashNotValidException e) {
            Log.setWarnLog(e.getMessage());
            respondDTO.setMessage(e.getMessage());
        }

        return new ResponseEntity<>(respondDTO, HttpStatus.OK);
    }


}
