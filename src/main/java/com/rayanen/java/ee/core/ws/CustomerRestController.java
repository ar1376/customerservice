package com.rayanen.java.ee.core.ws;

import com.rayanen.java.ee.core.dto.CustomerDTO;
import com.rayanen.java.ee.core.dto.RespondDTO;
import com.rayanen.java.ee.core.exceptions.*;
import com.rayanen.java.ee.core.facade.ICustomerFacade;
import com.rayanen.java.ee.core.log.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/customer")
public class CustomerRestController {

    private final ICustomerFacade customerFacade;

    private final Log Log;

    @Autowired
    public CustomerRestController(ICustomerFacade customerFacade, Log Log) {
        this.customerFacade = customerFacade;
        this.Log = Log;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/insert")
    public @ResponseBody
    ResponseEntity<RespondDTO> insert(@RequestBody CustomerDTO customerDTO) {
        RespondDTO respondDTO = new RespondDTO();
        try {
            customerFacade.insert(customerDTO);
            respondDTO.setMessage("Added Successfully");
        } catch (StoreFailedException e) {
            Log.setWarnLog(e.getMessage());
            respondDTO.setMessage("Can't Store Data !");
        } catch (CustomerIDDuplicateException e) {
            Log.setWarnLog(e.getMessage());
            respondDTO.setMessage("Customer ID Already Exist !");
        } catch (EmailNotValidException e) {
            Log.setWarnLog(e.getMessage());
            respondDTO.setMessage("Email Was Not Valid !");
        } catch (CustomerIDNotValidExeption e) {
            Log.setWarnLog(e.getMessage());
            respondDTO.setMessage("Customer ID Should Be 8 Digit !");
        } catch (LastNameNotValidException e) {
            Log.setWarnLog(e.getMessage());
            respondDTO.setMessage("Last Name Was Not Valid !");
        } catch (NameNotValidException e) {
            Log.setWarnLog(e.getMessage());
            respondDTO.setMessage("Name Was Not Valid !");
        }
        return new ResponseEntity<>(respondDTO, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/update")
    public @ResponseBody
    ResponseEntity<RespondDTO> update(@RequestBody CustomerDTO customerDTO) {
        RespondDTO respondDTO = new RespondDTO();
        try {
            customerFacade.editName(customerDTO, customerDTO.getName());
            respondDTO.setMessage("Edited Successfully");
        } catch (CustomerIDNotFoundException e) {
            Log.setWarnLog(e.getMessage());
            respondDTO.setMessage("Wrong Customer ID !");
        }
        try {
            customerFacade.editLastName(customerDTO, customerDTO.getLastName());
            respondDTO.setMessage("Edited Successfully");
        } catch (CustomerIDNotFoundException e) {
            Log.setWarnLog(e.getMessage());
            respondDTO.setMessage("Wrong Customer ID !");
        }
        return new ResponseEntity<>(respondDTO,HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/delete")
    public @ResponseBody
    ResponseEntity<RespondDTO> delete(@RequestBody CustomerDTO customerDTO) {
        RespondDTO respondDTO = new RespondDTO();
        try {
            customerFacade.remove(customerDTO.getCustomerID());
            respondDTO.setMessage("Removed Successfully");
        } catch (CustomerIDNotFoundException e) {
            Log.setWarnLog(e.getMessage());
            respondDTO.setMessage("Wrong Customer ID !");
        }
        return new ResponseEntity<>(respondDTO,HttpStatus.OK);
    }

}
